﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UpLang.Execution;

namespace Test {
  [TestClass]
  public class ScriptTest {

    [TestMethod]
    public void ParsingScript() {
      string code = @"
DefaultModule=up
; comment
[ini.merge|c:\\temp\\test.ini]
key1=value1
key2=value2
key3=value3
; comment
[set]=test
var1=value1
; comment
var2=`(text var1)`
; comment
";
      string[] lines = code.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
      Script script = new Script(lines);      
      var stream = script.SectionStream;
      stream.Reset();
      Assert.AreEqual(true, stream.HasNext);
      stream.Consume();

      Assert.AreEqual("let", stream.Current.Header.Title);
      Assert.AreEqual(1, stream.Current.KeyValues.Count);
      Assert.AreEqual("DefaultModule", stream.Current.KeyValues[0].Key);
      Assert.AreEqual("up", stream.Current.KeyValues[0].Value);
      stream.Consume();

      Assert.AreEqual("ini.merge", stream.Current.Header.Title);
      Assert.AreEqual(3, stream.Current.KeyValues.Count);
      Assert.AreEqual(1, stream.Current.Header.Param.Count);
      Assert.AreEqual("c:\\temp\\test.ini", stream.Current.Header.Param[0]);
      Assert.AreEqual(null, stream.Current.Header.Alias);
      Assert.AreEqual("key1", stream.Current.KeyValues[0].Key);
      Assert.AreEqual("value1", stream.Current.KeyValues[0].Value);
      Assert.AreEqual("key2", stream.Current.KeyValues[1].Key);
      Assert.AreEqual("value2", stream.Current.KeyValues[1].Value);
      Assert.AreEqual("key3", stream.Current.KeyValues[2].Key);
      Assert.AreEqual("value3", stream.Current.KeyValues[2].Value);
      stream.Consume();

      Assert.AreEqual("set", stream.Current.Header.Title);
      Assert.AreEqual(true, stream.HasNext);
      Assert.AreEqual(2, stream.Current.KeyValues.Count);
      Assert.AreEqual("test", stream.Current.Header.Alias);
      Assert.AreEqual("var1", stream.Current.KeyValues[0].Key);
      Assert.AreEqual("value1", stream.Current.KeyValues[0].Value);
      Assert.AreEqual("var2", stream.Current.KeyValues[1].Key);
      Assert.AreEqual("var1", stream.Current.KeyValues[1].Value);
      stream.Consume();
      Assert.AreEqual("end", stream.Current.Header.Title);
      stream.Consume();
      Assert.AreEqual(false, stream.HasNext);
      stream.Consume();
      stream.Consume();
      Assert.AreEqual(false, stream.HasNext);
      script.SectionStream.Reset();
    }
  }
}
