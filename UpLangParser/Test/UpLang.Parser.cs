﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UpLang.Parsing;
using UpLang.Parsing.Sentences;

namespace Test {
  [TestClass]
	public class ParserTest {
		[TestMethod]
		public void ParsingComment() {
			Assert.AreEqual("это комментарий", (Parser.ParseLine("  ;;;  это комментарий").Sentence as Comment).Text);
			Assert.AreEqual("key[=[value]]", (Parser.ParseLine(";;;;key[=[value]]").Sentence as Comment).Text);
			Assert.AreEqual("", (Parser.ParseLine(";;;;;         ").Sentence as Comment).Text);
			Assert.AreEqual(".", (Parser.ParseLine(";;;;;         .").Sentence as Comment).Text);
			Assert.AreEqual("это комментарий", (Parser.ParseLine("\t \t   \t;\t \t   \tэто комментарий").Sentence as Comment).Text);
		}
		[TestMethod]
		public void ParsingKeyValue() {
			KeyValue sentence;

      sentence = Parser.ParseLine("key1 \\ =value1").Sentence as KeyValue;
      Assert.AreEqual("key1  ", sentence.Key);
      Assert.AreEqual(true, sentence.HasAssign);
      Assert.AreEqual("value1", sentence.Value);

      sentence = Parser.ParseLine("key1 \" \"=value1").Sentence as KeyValue;
      Assert.AreEqual("key1  ", sentence.Key);
      Assert.AreEqual(true, sentence.HasAssign);
      Assert.AreEqual("value1", sentence.Value);

      sentence = Parser.ParseLine("   key1=value1").Sentence as KeyValue;
      Assert.AreEqual("key1", sentence.Key);
      Assert.AreEqual(true, sentence.HasAssign);
      Assert.AreEqual("value1", sentence.Value);

      sentence = Parser.ParseLine(" key2=").Sentence as KeyValue;
			Assert.AreEqual("key2", sentence.Key);
			Assert.AreEqual(true, sentence.HasAssign);
			Assert.AreEqual("", sentence.Value);

			sentence = Parser.ParseLine("  key3").Sentence as KeyValue;
			Assert.AreEqual("key3", sentence.Key);
			Assert.AreEqual(false, sentence.HasAssign);
			Assert.AreEqual(null, sentence.Value);

			sentence = Parser.ParseLine("\"   key1 \"=\"  value1     \"").Sentence as KeyValue;
			Assert.AreEqual("   key1 ", sentence.Key);
			Assert.AreEqual(true, sentence.HasAssign);
			Assert.AreEqual("  value1     ", sentence.Value);

			sentence = Parser.ParseLine("    \\t  key 2  =   value \\t .\\r\\n   . ").Sentence as KeyValue;
			Assert.AreEqual("\t  key 2", sentence.Key);
			Assert.AreEqual(true, sentence.HasAssign);
			Assert.AreEqual("value \t .\r\n   .", sentence.Value);

			sentence = Parser.ParseLine("\"=key\"\\=\"3=\"====value=3===").Sentence as KeyValue;
			Assert.AreEqual("=key=3=", sentence.Key);
			Assert.AreEqual(true, sentence.HasAssign);
			Assert.AreEqual("===value=3===", sentence.Value);

			sentence = Parser.ParseLine("\\[key4]\\==value4").Sentence as KeyValue;
			Assert.AreEqual("[key4]=", sentence.Key);
			Assert.AreEqual(true, sentence.HasAssign);
			Assert.AreEqual("value4", sentence.Value);

			sentence = Parser.ParseLine("\"[key5]=\"=value5").Sentence as KeyValue;
			Assert.AreEqual("[key5]=", sentence.Key);
			Assert.AreEqual(true, sentence.HasAssign);
			Assert.AreEqual("value5", sentence.Value);

			sentence = Parser.ParseLine("\" key \\\" 6 \"=\" value \\\" 6 \"").Sentence as KeyValue;
			Assert.AreEqual(" key \" 6 ", sentence.Key);
			Assert.AreEqual(true, sentence.HasAssign);
			Assert.AreEqual(" value \" 6 ", sentence.Value);

			sentence = Parser.ParseLine("\" key \\t 6 \"=\" value \\t 6 \"").Sentence as KeyValue;
			Assert.AreEqual(" key \t 6 ", sentence.Key);
			Assert.AreEqual(true, sentence.HasAssign);
			Assert.AreEqual(" value \t 6 ", sentence.Value);
		}
		[TestMethod]
		public void ParsingKeyValueWithEvaluations() {
			KeyValue sentence;

			sentence = Parser.ParseLine("  `(text name1)`  =   value`(- 4 3)`  ").Sentence as KeyValue;
			Assert.AreEqual("name1", sentence.Key);
			Assert.AreEqual(true, sentence.HasAssign);
			Assert.AreEqual("value1", sentence.Value);

			sentence = Parser.ParseLine("`(text name2=value2)`").Sentence as KeyValue;
			Assert.AreEqual("name2=value2", sentence.Key);
			Assert.AreEqual(false, sentence.HasAssign);
			Assert.AreEqual(null, sentence.Value);

			sentence = Parser.ParseLine("`(text name=3)`=value3").Sentence as KeyValue;
			Assert.AreEqual("name=3", sentence.Key);
			Assert.AreEqual(true, sentence.HasAssign);
			Assert.AreEqual("value3", sentence.Value);
		}
		[TestMethod]
		public void ParsingHeader() {
			Header sentence;

      sentence = Parser.ParseLine("[title]").Sentence as Header;
      Assert.AreEqual("title", sentence.Title);
      sentence = Parser.ParseLine("[title|]").Sentence as Header;
      Assert.AreEqual("title", sentence.Title);
      sentence = Parser.ParseLine("[title   ]").Sentence as Header;
      Assert.AreEqual("title", sentence.Title);
      sentence = Parser.ParseLine("[title  \\ ]").Sentence as Header;
      Assert.AreEqual("title   ", sentence.Title);
      sentence = Parser.ParseLine("[title  \" \"]").Sentence as Header;
      Assert.AreEqual("title   ", sentence.Title);

      sentence = Parser.ParseLine("[]=alias").Sentence as Header;
			Assert.AreEqual("", sentence.Title);
			Assert.AreEqual(0, sentence.Param.Count);
			Assert.AreEqual(true, sentence.HasAlias);
			Assert.AreEqual("alias", sentence.Alias);

			sentence = Parser.ParseLine(" [title | param1 | | param3 ]  =alias  ").Sentence as Header;
			Assert.AreEqual("title", sentence.Title);
			Assert.AreEqual(3, sentence.Param.Count);
			Assert.AreEqual("param1", sentence.Param[0]);
			Assert.AreEqual("", sentence.Param[1]);
			Assert.AreEqual("param3", sentence.Param[2]);
			Assert.AreEqual(true, sentence.HasAlias);
			Assert.AreEqual("alias", sentence.Alias);

			sentence = Parser.ParseLine("[\"  title  \"|\"param 1\"|\"param2|param2.1\"|param3]=\"bad alias\"").Sentence as Header;
			Assert.AreEqual("  title  ", sentence.Title);
			Assert.AreEqual(3, sentence.Param.Count);
			Assert.AreEqual("param 1", sentence.Param[0]);
			Assert.AreEqual("param2|param2.1", sentence.Param[1]);
			Assert.AreEqual("param3", sentence.Param[2]);
			Assert.AreEqual(true, sentence.HasAlias);
			Assert.AreEqual("bad alias", sentence.Alias);

			sentence = Parser.ParseLine("[\\|title\\  |\\|param1|   | \"\"| \" \"|\"\\t\\r\\n\"|param6|\"param[7]\"]").Sentence as Header;
			Assert.AreEqual("|title ", sentence.Title);
			Assert.AreEqual(7, sentence.Param.Count);
			Assert.AreEqual("|param1", sentence.Param[0]);
			Assert.AreEqual("", sentence.Param[1]);
			Assert.AreEqual("", sentence.Param[2]);
			Assert.AreEqual(" ", sentence.Param[3]);
			Assert.AreEqual("\t\r\n", sentence.Param[4]);
			Assert.AreEqual("param6", sentence.Param[5]);
			Assert.AreEqual("param[7]", sentence.Param[6]);
			Assert.AreEqual(false, sentence.HasAlias);
			Assert.AreEqual(null, sentence.Alias);

			sentence = Parser.ParseLine("[\\\"hello \\\" world\\\"]").Sentence as Header;
			Assert.AreEqual("\"hello \" world\"", sentence.Title);
			Assert.AreEqual(0, sentence.Param.Count);
			Assert.AreEqual(false, sentence.HasAlias);
			Assert.AreEqual(null, sentence.Alias);

			sentence = Parser.ParseLine("[\"hello \\\" world\"]").Sentence as Header;
			Assert.AreEqual("hello \" world", sentence.Title);
			Assert.AreEqual(0, sentence.Param.Count);
			Assert.AreEqual(false, sentence.HasAlias);
			Assert.AreEqual(null, sentence.Alias);

			sentence = Parser.ParseLine("[\"hello \\t world\"]").Sentence as Header;
			Assert.AreEqual("hello \t world", sentence.Title);
			Assert.AreEqual(0, sentence.Param.Count);
			Assert.AreEqual(false, sentence.HasAlias);
			Assert.AreEqual(null, sentence.Alias);

			//sentence = Parser.ParseLine("").Sentence as HeaderSentence;
			//Assert.AreEqual("", sentence.Header);
			//Assert.AreEqual(1, sentence.Param.Count);
			//Assert.AreEqual("", sentence.Param[0]);
			//Assert.AreEqual(true, sentence.HasAlias);
			//Assert.AreEqual("", sentence.Alias);

		}
		[TestMethod]
		public void ParsingHeaderWithEvaluations() {
			Header sentence;

			sentence = Parser.ParseLine("[`(text title)`|`(text param1|param1.1 param1.2 param 1.3)`|param2]").Sentence as Header;
			Assert.AreEqual("title", sentence.Title);
			Assert.AreEqual(2, sentence.Param.Count);
			Assert.AreEqual("param1|param1.1param1.2param1.3", sentence.Param[0]);
			Assert.AreEqual("param2", sentence.Param[1]);
			Assert.AreEqual(false, sentence.HasAlias);
			Assert.AreEqual(null, sentence.Alias);
		}
	}
}
