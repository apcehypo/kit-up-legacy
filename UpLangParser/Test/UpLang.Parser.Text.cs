﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UpLang.Parsing.Texts;

namespace Test {
  [TestClass]
	public class TextTest {
		[TestMethod]
		public void RawText() {
			RawText raw = new RawText("hello");
			Assert.AreEqual("hello", raw);
		}
		[TestMethod]
		public void EvalText() {
			Assert.AreEqual("11", new EvalText("(+ 5 6)"));
			Assert.AreEqual("11", new EvalText("(+ 5 (- 3 1) 4)"));
			Assert.AreEqual("sometext", new EvalText("(text sometext)"));
			Assert.AreEqual("some text", new EvalText("(text \"some text\")"));
		}
		[TestMethod]
		public void ChainOfRawText() {
			TextChain chain = new TextChain(
				new RawText("hello"),
				(RawText)"my",
				(RawText)"world"
			);
			Assert.AreEqual("hellomyworld", chain);
		}
	}
}
