﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UpLang.Execution;

namespace Test.StdLib {
  [TestClass]
  public class LetStatementTests {

    Executor executor = new Executor();

    [TestMethod]
    public void Assigning() {
      string code = @"
[let]
key1=value1
key2=
";
      Scope scope = new Scope(null);
      executor.Execute(code, scope);

      Assert.AreEqual("value1", scope.Get("key1"));
      Assert.AreEqual(string.Empty, scope.Get("key2"));
    }

    [TestMethod]
    public void ReAssigning() {
      string code = @"
[let]
key1=value1
key2=

[let]
key3=value3
key4=value4

[let]
key1
key2=value2
key4
key5=value5
";
      Scope scope = new Scope(null);
      executor.Execute(code, scope);

      Assert.IsFalse(scope.Has("key1"));
      Assert.AreEqual("value2", scope.Get("key2"));
      Assert.AreEqual("value3", scope.Get("key3"));
      Assert.IsFalse(scope.Has("key4"));
      Assert.AreEqual("value5", scope.Get("key5"));
    }

    [TestMethod]
    public void ReAssigningInSingleSection() {
      string code = @"
[let]
key1=value1
key2=
key3=value3
key4=value4
key1
key2=value2
key4
key5=value5
";
      Scope scope = new Scope(null);
      executor.Execute(code, scope);

      Assert.IsFalse(scope.Has("key1"));
      Assert.AreEqual("value2", scope.Get("key2"));
      Assert.AreEqual("value3", scope.Get("key3"));
      Assert.IsFalse(scope.Has("key4"));
      Assert.AreEqual("value5", scope.Get("key5"));
    }

    [TestMethod]
    public void ReAssigningWithEval() {
      string code1 = @"
[let]
key1=value1
key2=`key1`
key3=`key2`
key4=value4
";
      Scope scope = new Scope(null);
      executor.Execute(code1, scope);

      Assert.AreEqual("value1", scope.Get("key1"));
      Assert.AreEqual("value1", scope.Get("key2"));
      Assert.AreEqual("value1", scope.Get("key3"));
      Assert.AreEqual("value4", scope.Get("key4"));


      string code2 = @"
[let]
key4=`key4`
key5=`key4`
key6=`unknown`
";
      executor.Execute(code2, scope);

      Assert.AreEqual("value4", scope.Get("key4"));
      Assert.AreEqual("value4", scope.Get("key5"));
      Assert.AreEqual(string.Empty, scope.Get("key6"));
    }
  }
}
