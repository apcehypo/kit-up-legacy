﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UpLang.Execution;

namespace Test {
  [TestClass]
  public class ScopeTest {
    [TestMethod]
    public void FlatScope() {
      Scope scope = new Scope(null);
      Assert.IsFalse(scope.Owns("test"));
      scope.Set("test", "value");
      Assert.AreEqual("value", scope.Get("test"));
      Assert.IsFalse(scope.Owns("TEST"));
      scope.Set("test", "new value");
      Assert.AreEqual("new value", scope.Get("test"));
      scope.Set("test", "");
      Assert.AreEqual("", scope.Get("test"));
      scope.Set("test", null);
      Assert.IsFalse(scope.Owns("test"));

      Assert.AreEqual(string.Empty, scope.Get("not exists"));
    }

    [TestMethod]
    public void InheritedScope() {
      Scope parent = new Scope(null);
      Scope child = new Scope(parent);
      Assert.IsFalse(child.Owns("test"));
      Assert.IsFalse(parent.Owns("test"));

      child.Set("test", "child");
      Assert.IsFalse(parent.Owns("test"));
      Assert.IsTrue(child.Owns("test"));
      Assert.IsTrue(child.Has("test"));
      Assert.AreEqual("child", child.Get("test"));

      parent.Set("test", "parent");
      Assert.IsTrue(parent.Owns("test"));
      Assert.IsTrue(parent.Has("test"));
      Assert.AreEqual("parent", parent.Get("test"));
      Assert.AreEqual("child", child.Get("test"));

      child.Set("test", null);
      Assert.IsFalse(child.Owns("test"));
      Assert.IsTrue(child.Has("test"));
      Assert.IsTrue(parent.Owns("test"));
      Assert.IsTrue(parent.Has("test"));
      Assert.AreEqual("parent", parent.Get("test"));
      Assert.AreEqual("parent", child.Get("test"));

      parent.Set("test", null);
      Assert.IsFalse(parent.Owns("test"));
      Assert.IsFalse(child.Owns("test"));
      Assert.IsFalse(parent.Has("test"));
      Assert.IsFalse(child.Has("test"));
    }

    [TestMethod]
    public void NestedScopes() {
      Scope root = new Scope(null);
      Scope parent = new Scope(root);
      Scope boy = new Scope(parent);
      Scope girl = new Scope(parent);
      Scope child = new Scope(girl);

      root.Set("root", "root value");
      parent.Set("parent", "parent value");
      Assert.AreEqual("root value", parent.Get("root"));
      Assert.AreEqual("root value", boy.Get("root"));
      Assert.AreEqual("root value", girl.Get("root"));
      Assert.AreEqual("root value", child.Get("root"));
      Assert.AreEqual("parent value", boy.Get("parent"));
      Assert.AreEqual("parent value", girl.Get("parent"));
      Assert.AreEqual("parent value", child.Get("parent"));

      boy.Set("boy", "boy's secret");
      Assert.IsFalse(root.Has("boy"));
      Assert.IsFalse(parent.Has("boy"));
      Assert.IsFalse(girl.Has("boy"));
      Assert.IsFalse(child.Has("boy"));

      girl.Set("girl", "milk");
      Assert.AreEqual("milk", girl.Get("girl"));
      Assert.AreEqual("milk", child.Get("girl"));
      Assert.IsFalse(root.Has("girl"));
      Assert.IsFalse(parent.Has("girl"));
      Assert.IsFalse(boy.Has("girl"));
    }
  }
}
