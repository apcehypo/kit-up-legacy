﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UpLang.Parsing.Evaluations;

namespace Test {
  [TestClass]
	public class EvaluationTest {
		[TestMethod]
		public void ParsingAtoms() {
			string line;
			int index;
			Evaluation eval;

			line = "(fun1 a bb ccc dddd)";
			index = 0;
			eval = Evaluation.Parse(line, ref index);
			Assert.AreEqual("fun1", eval.Functor);
			Assert.AreEqual("a", eval.Arguments[0]);
			Assert.AreEqual("bb", eval.Arguments[1]);
			Assert.AreEqual("ccc", eval.Arguments[2]);
			Assert.AreEqual("dddd", eval.Arguments[3]);

			line = "(fun1  \"a\"   \"b b\"    \"c  c  c\"     \"dd    dd\")";
			index = 0;
			eval = Evaluation.Parse(line, ref index);
			Assert.AreEqual("fun1", eval.Functor);
			Assert.AreEqual("a", eval.Arguments[0]);
			Assert.AreEqual("b b", eval.Arguments[1]);
			Assert.AreEqual("c  c  c", eval.Arguments[2]);
			Assert.AreEqual("dd    dd", eval.Arguments[3]);

			line = "(fun1  \"a\"   \"b\\\"b\"    \"c \\\"c\\\" c\"     \"\\\"dd    dd\\\"\")";
			index = 0;
			eval = Evaluation.Parse(line, ref index);
			Assert.AreEqual("fun1", eval.Functor);
			Assert.AreEqual("a", eval.Arguments[0]);
			Assert.AreEqual("b\"b", eval.Arguments[1]);
			Assert.AreEqual("c \"c\" c", eval.Arguments[2]);
			Assert.AreEqual("\"dd    dd\"", eval.Arguments[3]);
		}

		[TestMethod]
		public void ParsingNested() {
			string line;
			int index;
			Evaluation eval;

			line = "(f fa (g gaa gbb) fccc (h))";
			index = 0;
			eval = Evaluation.Parse(line, ref index);
			Assert.AreEqual("f", eval.Functor);
			Assert.AreEqual("fa", eval.Arguments[0]);
			Assert.IsInstanceOfType(eval, typeof(Evaluation));
			Evaluation nested1 = eval.Arguments[1] as Evaluation;
			Assert.AreEqual("g", nested1.Functor);
			Assert.AreEqual("gaa", nested1.Arguments[0]);
			Assert.AreEqual("gbb", nested1.Arguments[1]);
			Assert.AreEqual("fccc", eval.Arguments[2]);
			Evaluation nested2 = eval.Arguments[3] as Evaluation;
			Assert.AreEqual("h", nested2.Functor);
			Assert.AreEqual(0, nested2.Arguments.Count);

			line = "(a (b (c (d (e (f))))))";
			index = 0;
			Evaluation nested;
			eval = Evaluation.Parse(line, ref index);
			Assert.AreEqual("a", eval.Functor);
			Assert.AreEqual(1, eval.Arguments.Count);
			Assert.IsInstanceOfType(eval.Arguments[0],typeof(Evaluation));
			nested = eval.Arguments[0] as Evaluation;
			Assert.AreEqual("b", nested.Functor);
			Assert.AreEqual(1, nested.Arguments.Count);
			Assert.IsInstanceOfType(nested.Arguments[0], typeof(Evaluation));
			nested = nested.Arguments[0] as Evaluation;
			Assert.AreEqual("c", nested.Functor);
			Assert.AreEqual(1, nested.Arguments.Count);
			Assert.IsInstanceOfType(nested.Arguments[0], typeof(Evaluation));
			nested = nested.Arguments[0] as Evaluation;
			Assert.AreEqual("d", nested.Functor);
			Assert.AreEqual(1, nested.Arguments.Count);
			Assert.IsInstanceOfType(nested.Arguments[0], typeof(Evaluation));
			nested = nested.Arguments[0] as Evaluation;
			Assert.AreEqual("e", nested.Functor);
			Assert.AreEqual(1, nested.Arguments.Count);
			Assert.IsInstanceOfType(nested.Arguments[0], typeof(Evaluation));
			nested = nested.Arguments[0] as Evaluation;
			Assert.AreEqual("f", nested.Functor);
			Assert.AreEqual(0, nested.Arguments.Count);
		}

		[TestMethod]
		public void ParsingSkipWhiteSpaces() {
			string line;
			int index;
			Evaluation eval;

			line = "( \t fun1   a\t\t\tbb   ccc    dddd\t  \t)";
			index = 0;
			eval = Evaluation.Parse(line, ref index);
			Assert.AreEqual("fun1", eval.Functor);
			Assert.AreEqual("a", eval.Arguments[0]);
			Assert.AreEqual("bb", eval.Arguments[1]);
			Assert.AreEqual("ccc", eval.Arguments[2]);
			Assert.AreEqual("dddd", eval.Arguments[3]);

			line = "(\t\t  f fa (\t g gaa gbb) fccc (h\t \t ))";
			index = 0;
			eval = Evaluation.Parse(line, ref index);
			Assert.AreEqual("f", eval.Functor);
			Assert.AreEqual("fa", eval.Arguments[0]);
			Assert.IsInstanceOfType(eval, typeof(Evaluation));
			Evaluation nested1 = eval.Arguments[1] as Evaluation;
			Assert.AreEqual("g", nested1.Functor);
			Assert.AreEqual("gaa", nested1.Arguments[0]);
			Assert.AreEqual("gbb", nested1.Arguments[1]);
			Assert.AreEqual("fccc", eval.Arguments[2]);
			Evaluation nested2 = eval.Arguments[3] as Evaluation;
			Assert.AreEqual("h", nested2.Functor);
			Assert.AreEqual(0, nested2.Arguments.Count);
		}
	}
}
