﻿using System;
using UpLang.Modules;
using System.ComponentModel.Composition;
using UpLang.Execution;

namespace SampleModule {
  [Export(typeof(IStatement))]
  [ExportMetadata("Name", "sampleStat")]
  public class SampleStatement : IStatement {
    public IExecutable Parse(ISectionStream stream) {
      Console.WriteLine("SampleStatement");
      return new SampleExecutable();      
    }
  }

  [Export(typeof(IExecutable))]
  public class SampleExecutable : IExecutable {
    public ExecutionResult Execute(IScope scope) {
      Console.WriteLine($"Hello from {nameof(SampleExecutable)}");
      return null;
    }
  }
}
