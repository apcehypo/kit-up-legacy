﻿using System;
using System.ComponentModel.Composition;
using UpLang.Modules;

namespace SampleModule {
  [Export(typeof(Module))]
  [ExportMetadata("Name", "sampleLib")]
  public class SampleModule : Module {
  }
}
