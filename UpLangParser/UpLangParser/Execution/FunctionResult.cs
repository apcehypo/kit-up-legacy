﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UpLang.Execution {
  public class FunctionResult {
    public string Value { get; set; }

    public static implicit operator string (FunctionResult result) {
      return result.Value;
    }

    public static implicit operator  FunctionResult (string value) {
      return new FunctionResult() { Value = value };
    }
  }
}
