﻿using System;
using System.Collections.Generic;

namespace UpLang.Execution {
  /// <summary>
  /// Исполняет операторы, хранящиеся в скрипте.
  /// Обеспечивает хранение переменных с учётом контекста.
  /// </summary>
  public class Executor {
    public Stack<IScope> ScopeTrace = new Stack<IScope>();

    public Executor() {
      Scope root = new Scope(null);
      ScopeTrace.Push(root);
    }
		public void Execute(string code, IScope scope) {
      string[] lines = code.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
      Script script = new Script(lines);
      script.Execute(scope);
    }

    public void Execute(string code) {
      Execute(code, ScopeTrace.Peek());
    }
	}
}
