﻿using System;
using System.Collections.Generic;

namespace UpLang.Execution {
  public class Scope : IScope {
    
    private IScope parent;

    private Dictionary<string, string> variables;

    public Scope(IScope parent) {
      this.parent = parent;
      this.variables = new Dictionary<string, string>();      
    }

    public bool HasParent => parent != null;

    public bool Has(string name) {
      return Owns(name) || (HasParent && parent.Has(name));
    }

    public bool Owns(string name) {
      return variables.ContainsKey(name);
    }

    public string Get(string name) {
      if (Owns(name)) return variables[name];
      else if (HasParent) return parent.Get(name);
      else return string.Empty;
    }

    public void Set(string name, string value) {
      if (value != null) {
        variables[name] = value;
      }
      else {
        variables.Remove(name);
      }
    }
  }

}
