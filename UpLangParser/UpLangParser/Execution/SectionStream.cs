﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;

namespace UpLang.Execution {
  public class SectionStream : ISectionStream {
    private List<Section> sections = new List<Section>();
    private int current = -1;

    internal void Add(Section section) {
      sections.Add(section);
    }

    public Section Current {
      get {
        if (current >= 0 && current < sections.Count) return sections[current];
        else return null;
      }
    }

    public bool Consume() {
      if (current < sections.Count) {
        current++;
        return true;
      }
      else {
        return false;
      }
    }

    public void Reset() {
      current = -1;
    }

    public bool HasNext {
      get {
        return current < sections.Count;
      }
    }
  }
}
