﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UpLang.Modules;
using UpLang.Parsing;
using UpLang.Parsing.Sentences;
using UpLang.StdLib;

namespace UpLang.Execution {
  /// <summary>
  /// Инкапсулирует секции одного файла и конфигурацию, определённую в файле.
  /// Скрипт нейвно оборачивается в блок [begin]...[end],
  /// а безымянная секция, определяющая настройки, становится оператором [let]
  /// и помещается в начало списка секций.
  /// </summary>
  public class Script : IStatement, IExecutable {

    public IExecutable Root { get; private set; }

    public SectionStream SectionStream { get; private set; }

    public Script(string[] lines) {
      SectionStream = ParseLines(lines);
      SectionStream.Add(new Section(new Header("[end]")));
      Root = Parse(SectionStream);
    }

    private SectionStream ParseLines(string[] lines) {
      var stream = new SectionStream();
      var tuples = lines.Select(x => Parser.ParseLine(x));
      //строки безымянной секции попадают в конфигурацию - секцию [let]
      Section target = new Section(new Header("[let]"));
      stream.Add(target);
      foreach (var tuple in tuples) {
        if (tuple == null) continue;
        switch (tuple.Kind) {
          case SentenceType.Comment:
            //ignore
            break;
          case SentenceType.Header:
            target = new Section(tuple.Sentence as Header);
            stream.Add(target);
            break;
          case SentenceType.KeyValue:
            target.Add(tuple.Sentence as KeyValue);
            break;
          default:
            break;
        }
      }
      return stream;
    }

    public IExecutable Parse(ISectionStream stream) {
      stream.Consume(); //потребляем виртуальный [begin]
      return new BeginEndStatement().Parse(stream);
    }

    public ExecutionResult Execute(IScope scope) {
      return Root.Execute(scope);
    }
  }
}
