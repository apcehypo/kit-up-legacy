﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UpLang.Execution {
  public interface IScope {
    void Set(string name, string value);
    string Get(string name);
    bool Has(string name);
    bool Owns(string name);
  }
}
