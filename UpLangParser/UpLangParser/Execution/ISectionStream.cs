﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace UpLang.Execution {
  public interface ISectionStream {

    Section Current { get; }

    bool Consume();

    bool HasNext { get; }

    void Reset();
  }
}