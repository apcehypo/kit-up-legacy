﻿using System;
using System.Collections.ObjectModel;
using UpLang.Modules;
using UpLang.Parsing.Sentences;

namespace UpLang.Execution {
  /// <summary>
  /// Инкапсулирует содержание заголовка и ключей секции.
  /// Предоставляет интерфейс парсинга.
  /// </summary>
  public class Section : IStatement {

    public Section(Header header) {
      Header = header;
      KeyValues = new Collection<KeyValue>();
    }

    public Header Header { get; private set; }

    public Collection<KeyValue> KeyValues { get; private set; }

    public void Add(KeyValue sentence) {
      KeyValues.Add(sentence);
    }

    /// <summary>
    /// Абстрактный фабричный парсер, принимающий решение, какой парсер использовать.
    /// </summary>
    /// <param name="stream">Не используется. Данная секция сама является stream.Current.</param>
    /// <returns></returns>
    public IExecutable Parse(ISectionStream stream) {
      var toConsume = stream.Current;
      try {
        var statement = Program.Repository.GetStatement(Header.Title);
        
        return statement.Parse(stream);
      }
      catch (Exception) {
        //TODO: отреагировать
        return null;
      }
      finally {
        // Если сторонний парсер не продвинулся в потоке (по недоработке), делаем это за него.
        //TODO: отреагировать
        if (toConsume == stream.Current) {
          stream.Consume();
        }
      }
    }

    public override string ToString() {
      return Header.Line;
    }
  }
}
