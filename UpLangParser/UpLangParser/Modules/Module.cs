﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Reflection;

namespace UpLang.Modules {

  public interface IModuleMetadata {
    string Name { get; }
  }

  public abstract class Module {
    public IEnumerable<Lazy<IFunction, IFunctionMetadata>> FunctionsImports { get; set; }

    public Dictionary<string, Lazy<IFunction>> Functions = new Dictionary<string, Lazy<IFunction>>();

    public IEnumerable<Lazy<IStatement, IStatementMetadata>> StatementsImports { get; set; }

    public Dictionary<string, Lazy<IStatement>> Statements = new Dictionary<string, Lazy<IStatement>>();

    private Assembly self;

    public virtual string Copyright {
      get {
        return new Lazy<string>(() => ((AssemblyCopyrightAttribute)self.GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false).FirstOrDefault()).Copyright).Value;
      }
    }

    public virtual Version Version {
      get {
        //return new Lazy<Version>(() => self.GetName().Version).Value;
        return self.GetName().Version;
      }
    }

    public Module() {
      self = this.GetType().Assembly;
      var catalog = new AggregateCatalog();
      catalog.Catalogs.Add(new AssemblyCatalog(this.GetType().Assembly));
      var cc = new CompositionContainer(catalog);
      FunctionsImports = cc.GetExports<IFunction, IFunctionMetadata>();
      StatementsImports = cc.GetExports<IStatement, IStatementMetadata>();

      foreach (var item in FunctionsImports) {
        if (!Functions.ContainsKey(item.Metadata.Name)) {
          Functions.Add(item.Metadata.Name, new Lazy<IFunction>(() => item.Value));
        }
        else {
          throw new AmbiguousFunctionDifinitionException(item.Metadata.Name, this, Functions[item.Metadata.Name].Value, item.Value);
        }
      }

      foreach (var item in StatementsImports) {
        if (!Statements.ContainsKey(item.Metadata.Name)) {
          Statements.Add(item.Metadata.Name, new Lazy<IStatement>(() => item.Value));
        }
        else {
          throw new AmbiguousStatementDifinitionException(item.Metadata.Name, this, Statements[item.Metadata.Name].Value, item.Value);
        }
      }
    }
  }
}
