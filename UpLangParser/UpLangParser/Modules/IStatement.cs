﻿using UpLang.Execution;

namespace UpLang.Modules {
  public interface IStatementMetadata {
    string Name { get; }
  }

  public interface IStatement {
    IExecutable Parse(ISectionStream stream);
  }

}
