﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UpLang.Execution;

namespace UpLang.Modules {
  public interface IExecutable {
    ExecutionResult Execute(IScope scope);
  }
}
