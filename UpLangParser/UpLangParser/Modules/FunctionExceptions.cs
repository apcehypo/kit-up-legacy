﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UpLang.Modules {

  [Serializable]
  public class WrongArgsCountException : Exception {
    public IFunction Function { get; }
    
    public int GotCount { get; }

    public WrongArgsCountException(IFunction function, int gotCount) {
      Function = function;
      GotCount = gotCount;
    }
  }
}
