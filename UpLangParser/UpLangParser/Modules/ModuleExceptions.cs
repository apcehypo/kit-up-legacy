﻿using System;

namespace UpLang.Modules {

  [Serializable]
  public class AmbiguousDifinitionException : Exception {
    public string Name { get; }
    public AmbiguousDifinitionException(string name) {
      Name = name;
    }
  }

  [Serializable]
  public class AmbiguousModuleDifinitionException : Exception {
    public Module[] Modules { get; }
    public AmbiguousModuleDifinitionException(string name, params Module[] modules) : base(name) {
      Modules = modules;
    }
  }

  [Serializable]
  public class AmbiguousFunctionDifinitionException : AmbiguousDifinitionException {
    public Module Module { get; }
    public IFunction[] Functions { get; }
    public AmbiguousFunctionDifinitionException(string name, Module module, params IFunction[] functions) : base(name) {
      Module = module;
      Functions = functions;
    }
  }

  [Serializable]
  public class AmbiguousStatementDifinitionException : AmbiguousDifinitionException {
    public Module Module { get; }
    public IStatement[] Statements { get; }
    public AmbiguousStatementDifinitionException(string name, Module module, params IStatement[] statements) : base(name) {
      Module = module;
      Statements = statements;
    }
  }
}
