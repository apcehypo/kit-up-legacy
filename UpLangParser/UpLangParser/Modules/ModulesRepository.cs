﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;

namespace UpLang.Modules {
  /// <summary>
  /// Инкапсулирует работу с плагинами - модули с операторами и функциями.
  /// Стандартная библиотека обрабатывается так же как и другие модули.
  /// </summary>
  public class ModuleRepository {

    [ImportMany]
    internal IEnumerable<Lazy<Module, IModuleMetadata>> Modules { get; set; }

    [ImportMany]
    internal IEnumerable<Lazy<IExecutable>> Executables { get; set; }

    private Dictionary<string, Module> modules = new Dictionary<string, Module>();

    public string DefaultModule = "std";

    /// <summary>
    /// Разрешает имя функции, находя её в модуле.
    /// </summary>
    /// <param name="name"></param>
    /// <returns>Объект-функцию.</returns>
    public IFunction GetFunction(string name) { //TODO: TEST
      if (string.IsNullOrEmpty(name)) throw new ArgumentNullException();
      string moduleName;
      string functionName;
      if (name.Contains(".")) {
        int dotIndex = name.IndexOf('.');
        moduleName = name.Substring(0, dotIndex);
        functionName = name.Substring(dotIndex + 1);
      }
      else {
        moduleName = DefaultModule;
        functionName = name;
      }
      if (modules.ContainsKey(moduleName)) {
        var module = modules[moduleName];
        if (module.Functions.ContainsKey(functionName)) {
          return module.Functions[functionName].Value;
        }
        else {
          throw new Exception();//TODO: специальное исключение
        }
      }
      else {
        throw new Exception();//TODO: специальное исключение
      }
    }

    //HACK: copy-paste GetFunction
    public IStatement GetStatement(string name) { //TODO: TEST!!!
      if (string.IsNullOrEmpty(name)) throw new ArgumentNullException();
      string moduleName;
      string statementName;
      if (name.Contains(".")) {
        int dotIndex = name.IndexOf('.');
        moduleName = name.Substring(0, dotIndex);
        statementName = name.Substring(dotIndex + 1);
      }
      else {
        moduleName = DefaultModule;
        statementName = name;
      }
      if (modules.ContainsKey(moduleName)) {
        var module = modules[moduleName];
        if (module.Statements.ContainsKey(statementName)) {
          return module.Statements[statementName].Value;
        }
        else {
          throw new Exception();//TODO: специальное исключение
        }
      }
      else {
        throw new Exception();//TODO: специальное исключение
      }
    }

    public ModuleRepository() {
      var self = this.GetType().Assembly;

      var catalog = new AggregateCatalog();
      //импортируем из текущей сборки - StdLib
      catalog.Catalogs.Add(new AssemblyCatalog(self));
      //импортируем dll-ки из папки рядом с .exe
      catalog.Catalogs.Add(new DirectoryCatalog(Path.GetDirectoryName(self.Location), "*.dll"));

      var cc = new CompositionContainer(catalog);
      try {
        cc.ComposeParts(this);
      }
      catch (CompositionException compositionException) {
        Console.WriteLine(compositionException.ToString());
      }

      foreach (var item in Modules) {
        if (!modules.ContainsKey(item.Metadata.Name)) {
          modules.Add(item.Metadata.Name, item.Value);
        }
        else {
          throw new AmbiguousModuleDifinitionException(item.Metadata.Name, modules[item.Metadata.Name], item.Value);
        }
      }
    }
  }
}
