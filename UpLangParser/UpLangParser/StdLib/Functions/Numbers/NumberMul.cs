﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using UpLang.Execution;
using UpLang.Modules;

namespace UpLang.StdLib {

  [Export(typeof(IFunction))]
  [ExportMetadata("Name", "*")]
  public class NumberMul : IFunction {
    public bool Memoizable { get; } = true;

    public int MinimalArity { get; } = 1;

    public int MaximalArity { get; } = int.MaxValue;

    public FunctionResult Execute(IScope scope, params string[] args) {
      if (args.Length == 0) throw new WrongArgsCountException(this, args.Length);
      decimal result = 1m;
      decimal value;
      foreach (var arg in args) {
        if (!decimal.TryParse(arg, out value)) throw new ArgumentException();
        result *= value;

      }
      return result.ToString();
    }
  }

}
