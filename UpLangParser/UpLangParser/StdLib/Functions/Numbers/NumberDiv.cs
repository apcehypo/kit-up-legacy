﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using UpLang.Execution;
using UpLang.Modules;

namespace UpLang.StdLib.Functions {

  [Export(typeof(IFunction))]
  [ExportMetadata("Name", "/")]
  public class NumberDiv : IFunction {
    public bool Memoizable { get; } = true;

    public int MinimalArity { get; } = 1;

    public int MaximalArity { get; } = int.MaxValue;

    public FunctionResult Execute(IScope scope, params string[] args) {
      if (args.Length == 0) throw new WrongArgsCountException(this, args.Length);
      if (args.Length == 1) {
        //invert number
        decimal value;
        if (!decimal.TryParse(args[0], out value)) throw new ArgumentException();
        return (1 / value).ToString();
      }
      else {
        //substraction
        decimal result;
        if (!decimal.TryParse(args[0], out result)) throw new ArgumentException();
        decimal value;
        for (int i = 1; i < args.Length; i++) {
          if (!decimal.TryParse(args[i], out value)) throw new ArgumentException();
          result /= value;
        }
        return result.ToString();
      }
    }
  }

}
