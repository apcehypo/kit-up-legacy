﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using UpLang.Execution;
using UpLang.Modules;


namespace UpLang.StdLib.Functions {

  [Export(typeof(IFunction))]
  [ExportMetadata("Name", "get")]
  public class GetFunction : IFunction {

    public bool Memoizable { get; } = false;

    public int MinimalArity { get; } = 1;

    public int MaximalArity { get; } = 1;

    public FunctionResult Execute(IScope scope, params string[] args) {
      if (args.Length < MinimalArity || args.Length > MaximalArity) throw new WrongArgsCountException(this, args.Length);
      
      return scope.Get(args[0]);
    }
  }

}

