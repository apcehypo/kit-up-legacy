﻿using System.Linq;

namespace UpLang.StdLib.Functions.Text {
  public static class StringCommonTools {
    public static string Concat(params string[] args) {
      if (args.Length == 0)
        return string.Empty;
      if (args.Length == 1)
        return args[0];
      return args.Aggregate(string.Empty, (acc, arg) => acc + arg);
    }
  }
}