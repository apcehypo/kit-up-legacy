﻿using System.ComponentModel.Composition;
using System.Linq;
using UpLang.Execution;
using UpLang.Modules;

namespace UpLang.StdLib.Functions.Text {
  [Export(typeof(IFunction))]
  [ExportMetadata("Name", "text")]
  public class StringText : IFunction {
    public bool Memoizable { get; } = true;
    public int MinimalArity { get; } = 0;
    public int MaximalArity { get; } = 1;
    public FunctionResult Execute(IScope scope, params string[] args) {
      return StringCommonTools.Concat(args);
    }
  }
}