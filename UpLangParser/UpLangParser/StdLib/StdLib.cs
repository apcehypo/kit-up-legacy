﻿using System.ComponentModel.Composition;
using UpLang.Modules;

namespace UpLang.StdLib {
  [Export(typeof(Module))]
  [ExportMetadata("Name", "std")]
  public class StdLib : Module {
  }
}
