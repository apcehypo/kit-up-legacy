﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Modules;

namespace UpLang.StdLib {
  /// <summary>
  /// Блок операторов:
  /// [begin]
  ///   [...]
  ///   ...
  /// [end]
  /// </summary>

  [Export(typeof(IStatement))]
  [ExportMetadata("Name", "begin")]
  public class BeginEndStatement : IStatement {
    public IExecutable Parse(ISectionStream stream) {
      Console.WriteLine("BeginEndParsing");
      var executables = new BeginEndExecutable();
      while (stream.HasNext) {
        if (stream.Current.Header.Title == "end") {
          stream.Consume();
          break;
        }
        else {
          executables.Executables.Add(stream.Current.Parse(stream));
        }
      }
      return executables;
    }
  }

  [Export(typeof(IExecutable))]
  public class BeginEndExecutable : IExecutable {

    internal List<IExecutable> Executables { get; private set; }

    internal BeginEndExecutable() {
      Executables = new List<IExecutable>();
    }

    public ExecutionResult Execute(IScope scope) {
      Program.Executor.ScopeTrace.Push(scope);
      var results = new CompositeExecutionResult();
      foreach (var executable in Executables) {
        results.Add(executable.Execute(scope));
      }
      Program.Executor.ScopeTrace.Pop();
      return results;
    }
  }
}

