﻿using System;
using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Modules;

namespace UpLang.StdLib {
  /// <summary>
  /// Блок операторов:
  /// [let]
  /// key1=value
  /// key2=
  /// key3
  /// ...
  /// Присваивает key значение value в текущем контексте.
  /// key1 - будет value
  /// key2 - будет пустая строка
  /// key3 - будет удалён из контекста
  /// </summary>

  [Export(typeof(IStatement))]
  [ExportMetadata("Name", "let")]
  public class LetStatement : IStatement {
    public IExecutable Parse(ISectionStream stream) {
      Section section = stream.Current;
      stream.Consume();            
      return new LetExecutable(section);
    }
  }

  [Export(typeof(IExecutable))]
  public class LetExecutable : IExecutable {

    private Section section;

    internal LetExecutable(Section section) {
      this.section = section;
    }

    public ExecutionResult Execute(IScope scope) {
      //TODO: поддержка хэшей      
      foreach (var line in section.KeyValues) {
        if (line.HasAssign) {
          scope.Set(line.Key, line.Value);
        }
        else {
          scope.Set(line.Key, null);
        }
      }
      return null;
    }
  }
}

