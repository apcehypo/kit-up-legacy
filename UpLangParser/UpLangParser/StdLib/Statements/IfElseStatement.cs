﻿using System;
using System.ComponentModel.Composition;
using UpLang.Execution;
using UpLang.Modules;

namespace UpLang.StdLib {
  /// <summary>
  /// Условный оператор:
  /// [if|condition]
  ///   [block]
  /// [orif|condition] //не обязательно
  ///   [block]
  /// [else] //не обязательно
  ///   [block]
  /// 
  /// Варианты с отрицанием:
  /// вместо if - ifnot
  /// вместо orif - orifnot
  /// </summary>

  [Export(typeof(IStatement))]
  [ExportMetadata("Name", "if")]
  public class IfElseStatement : IStatement {
    public IExecutable Parse(ISectionStream stream) {
      Console.WriteLine("IfElseParsing");
      return null;
    }
  }

  [Export(typeof(IStatement))]
  [ExportMetadata("Name", "ifnot")]
  public class IfNotElseStatement : IStatement {
    public IExecutable Parse(ISectionStream stream) {
      Console.WriteLine("IfNotElseParsing");
      return null;
    }
  }


  [Export(typeof(IExecutable))]
  public class IfElseExecutable : IExecutable {
    public ExecutionResult Execute(IScope scope) {
      Console.WriteLine("IfElseExecuting");
      return null;
    }
  }
}

