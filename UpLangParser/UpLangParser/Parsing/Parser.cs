﻿using System;
using System.Linq;
using UpLang.Parsing.Sentences;

namespace UpLang.Parsing {
  /// <summary>
  /// 
  /// </summary>
  public static class Parser {
		internal static char[] WhiteSpaceChars = { ' ', '\t' };
		public const char CommentStartChar = ';';
		public const char HeaderStartChar = '[';
		public const char HeaderEndChar = ']';
		public const char SplitterChar = '|';
		public const char AssignChar = '=';
		public const char QuoteChar = '"';
		public const char SubstitutionChar = '`';
		public const char EscapeChar = '\\';

		public static Line ParseLine(string sourceLine) {
			if (sourceLine == null) throw new ArgumentNullException();
			string line = sourceLine.Trim(WhiteSpaceChars);
			if (!string.IsNullOrEmpty(line)) {
				switch (line[0]) {
					case CommentStartChar:
						return new Line { Kind = SentenceType.Comment, Sentence = new Comment(line) };
					case HeaderStartChar:
						return new Line { Kind = SentenceType.Header, Sentence = new Header(line) };
					default:
						return new Line { Kind = SentenceType.KeyValue, Sentence = new KeyValue(line) };
				}
			}
			else {
				return null;
			}
		}

		public static void SkipNextWhiteSpaces(this string line, ref int index) {
			if (string.IsNullOrEmpty(line)) throw new ArgumentNullException();
			if (index >= line.Length) throw new IndexOutOfRangeException();
			while (index < line.Length - 1 && Parser.WhiteSpaceChars.Contains(line[index + 1])) index++;
		}

		public static void SkipWhiteSpaces(this string line, ref int index) {
			if (string.IsNullOrEmpty(line)) throw new ArgumentNullException();
			if (index >= line.Length) throw new IndexOutOfRangeException();
			while (index < line.Length && Parser.WhiteSpaceChars.Contains(line[index])) index++;
		}

		public static char GetEscapedChar(this string line, int index) {
			if (string.IsNullOrEmpty(line)) throw new ArgumentNullException();
			if (index >= line.Length) throw new IndexOutOfRangeException();
			switch (line[index]) {
				case 't':
					return '\t';
				case 'r':
					return '\r';
				case 'n':
					return '\n';
				default:
					return line[index];
			}
		}


	}

}
