
namespace UpLang.Parsing.Texts {
	/// <summary>
	///
	/// </summary>
	public sealed class RawText : Text {
		public RawText(string text) {
			this.SourceText = text;
		}
		protected override string Value {
			get { return SourceText; }
		}

		public static explicit operator RawText(string text) {
			return new RawText(text);
		}

		public static RawText ToRawText(string text) {
			return new RawText(text);
		}

		public static explicit operator RawText(char symbol) {
			return new RawText(symbol.ToString());
		}
		public static RawText ToRawText(char symbol) {
			return new RawText(symbol.ToString());
		}
	}
}