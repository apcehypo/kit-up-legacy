using System;
using System.Collections.ObjectModel;
using System.Text;

namespace UpLang.Parsing.Texts {
  /// <summary>
  ///
  /// </summary>
  public sealed class TextChain : Text {
		public Collection<Text> Parts { get; private set; }
		public TextChain(params Text[] parts) {
			this.Parts = new Collection<Text>(parts);
		}

		public void Append(params Text[] parts) {
			if (parts == null) throw new ArgumentNullException();
			foreach (var part in parts) {
				this.Parts.Add(part);
			}
		}

		protected override string Value {
			get {
				StringBuilder sb = new StringBuilder();
				foreach (var part in Parts) {
					sb.Append(part);
				}
				return sb.ToString();
			}
		}
	}
}