﻿namespace UpLang.Parsing.Texts {

  /// <summary>
  /// 
  /// </summary>
  public abstract class Text {
		protected string SourceText { get; set; }
		protected abstract string Value { get; }

		public static TextChain operator +(Text left, Text right) {
			return new TextChain(left, right);
		}

		public TextChain Add(Text that) {
			return this + that;
		}

		public bool Equals(Text that) {
			if (null != that) {
				return this.Value == that.Value;
			}
			else {
				return false;
			}
		}

		public static bool operator ==(Text left, Text right) {
			if ((object)left == null) return (object)right == null;
			else return left.Equals(right);
		}

		public static bool operator !=(Text left, Text right) {
			if ((object)left == null) return (object)right != null;
			else return !left.Equals(right);
		}

		public override bool Equals(object obj) {
			Text that = obj as Text;
			return this.Equals(that);
		}

		public static implicit operator string(Text text) {
			if ((object)text != null) {
				return text.Value;
			}
			else {
				return null;
			}
		}

		public override string ToString() {
			return Value;
		}

		public override int GetHashCode() {
			return Value.GetHashCode();
		}
	}

}

