using System;
using UpLang.Parsing.Evaluations;
namespace UpLang.Parsing.Texts {
  /// <summary>
  ///
  /// </summary>
  public class EvalText : Text {
		private string memoized = null;
		private Evaluation evaluation = null;

		public EvalText(string text) {
			this.SourceText = text;
			if (string.IsNullOrEmpty(text)) {
				memoized = "";
			}
			else {
				if (text[0] == Evaluation.BracketStartChar) {
					//������� ���������
					// `(fun arg1 arg2)`
					// `(fun1 arg1 (fun1 arg21 arg22) arg3)`
					int index = 0;
					evaluation = Evaluation.Parse(text, ref index);
				}
				else {
					//�������� ����������
					// `name` -> `(get name)`
					// `name1 name2 name3` -> `(get name1)`
					//�� �������: ����� ����� ������� get � ����������� �����������, ������������ ������ ������������ ���������� �� �������������

					var words = text.Split(Parser.WhiteSpaceChars, StringSplitOptions.RemoveEmptyEntries);
					//!!!
					evaluation = new Evaluation((RawText)"get", (RawText)words[0]);
				}
			}
		}

		protected override string Value {
			get {
				if (memoized == null) {
					if (evaluation.canMemoize) {
						memoized = evaluation.Value;
						return memoized;
					}
					else {
						return evaluation.Value;
					}
				}
				else {
					return memoized;
				}
			}
		}

		public static explicit operator EvalText(string text) {
			return new EvalText(text);
		}

		public static EvalText FromString(string text) {
			return new EvalText(text);
		}

	}
}