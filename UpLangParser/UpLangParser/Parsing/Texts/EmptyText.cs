
namespace UpLang.Parsing.Texts {
	/// <summary>
	///
	/// </summary>
	public sealed class EmptyText : Text {
		protected override string Value {
			get { return ""; }
		}
	}
}