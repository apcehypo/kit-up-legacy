﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UpLang.Modules;

namespace UpLang.Parsing.Evaluations {
  /// <summary>
  /// 
  /// </summary>
  public class Evaluation : Expression {
    public const char BracketStartChar = '(';
    public const char BracketEndChar = ')';

    private Evaluation() { }

    public Evaluation(string functor, params string[] arguments) {
      this.Functor = (Atom)functor;
      this.Arguments = new ReadOnlyCollection<Expression>(arguments.Select(x => (Expression)(Atom)x).ToList());
    }

    public Atom Functor { get; protected set; }

    public ReadOnlyCollection<Expression> Arguments { get; private set; }


    public bool canMemoize { get; private set; }
    private string memoized = null;

    private enum LogicalState : int { Functor, AnalizingArgument, Argument }
    private enum PhysicalState : int { Atom, AfterEscape, InQuotes }

    public static Evaluation Parse(string line, ref int i) {
      //сюда должны поступать только выражения в скобках
      if (line[i] != Evaluation.BracketStartChar) return null;

      LogicalState logicalState = LogicalState.Functor;
      Stack<LogicalState> logicalTrace = new Stack<LogicalState>();
      PhysicalState physicalState = PhysicalState.Atom;
      PhysicalState previousPhysicalState = PhysicalState.Atom;
      StringBuilder sb = new StringBuilder();
      List<Expression> argument = new List<Expression>();
      Evaluation result = new Evaluation();

      //первый символ равен '('
      i++;
      line.SkipWhiteSpaces(ref i);
      for (; i < line.Length; i++) {
        switch (logicalState) {
          case LogicalState.Functor:
            if (IsAtomEnd(line, i)) {
              result.Functor = (Atom)sb.ToString();
              sb.Clear();
              logicalState = LogicalState.AnalizingArgument;
              i--; //не прыгаем на следующее выражение
            }
            else {
              sb.Append(line[i]);
            }
            break;
          case LogicalState.AnalizingArgument:
            line.SkipWhiteSpaces(ref i);
            switch (line[i]) {
              case BracketEndChar:
                //больше аргументов нет
                result.Arguments = new ReadOnlyCollection<Expression>(argument);
                return result; //закончить разбор
              case BracketStartChar:
                //вложенное вычисляемое выражение
                Evaluation nested = Evaluation.Parse(line, ref i);
                argument.Add(nested);
                logicalState = LogicalState.AnalizingArgument;
                break;
              default:
                //атом
                logicalState = LogicalState.Argument;
                i--; //пусть разбираются в своём состоянии
                break;
            }
            break;
          case LogicalState.Argument:
            switch (physicalState) {
              case PhysicalState.Atom:
                switch (line[i]) {
                  case BracketEndChar:
                    //последний аргумент
                    argument.Add((Atom)sb.ToString());
                    result.Arguments = new ReadOnlyCollection<Expression>(argument);
                    return result; //закончить разбор
                  case Parser.QuoteChar:
                    physicalState = PhysicalState.InQuotes;
                    break;
                  case Parser.EscapeChar:
                    previousPhysicalState = physicalState;
                    physicalState = PhysicalState.AfterEscape;
                    break;
                  default:
                    if (IsAtomEnd(line, i)) {
                      argument.Add((Atom)sb.ToString());
                      sb.Clear();
                      logicalState = LogicalState.AnalizingArgument;
                    }
                    else {
                      sb.Append(line[i]);
                    }
                    break;
                }
                break;
              case PhysicalState.AfterEscape:
                physicalState = previousPhysicalState;
                sb.Append(line.GetEscapedChar(i));
                break;
              case PhysicalState.InQuotes:
                switch (line[i]) {
                  case Parser.QuoteChar:
                    physicalState = PhysicalState.Atom;
                    break;
                  case Parser.EscapeChar:
                    previousPhysicalState = physicalState;
                    physicalState = PhysicalState.AfterEscape;
                    break;
                  default:
                    sb.Append(line[i]);
                    break;
                }
                break;
              default:
                throw new NotImplementedException("ENUM PhysicalState");
            }
            break;
          default:
            throw new NotImplementedException("ENUM LogicalState");
        }
      }

      return result;
    }

    private static bool IsAtomEnd(string line, int i) {
      return line[i] == Evaluation.BracketEndChar || Parser.WhiteSpaceChars.Contains(line[i]);
    }

    public override string Value {
      get {
        if (canMemoize) {
          if (memoized == null) memoized = this.Evaluate();
          return memoized;
        }
        else {
          return this.Evaluate();
        }
      }
    }
    private string Evaluate() {
      return Program.Repository.GetFunction(this.Functor).Execute(Program.Executor.ScopeTrace.Peek(), this.Arguments.Select(x => (string)x).ToArray());
    }
  }
}
