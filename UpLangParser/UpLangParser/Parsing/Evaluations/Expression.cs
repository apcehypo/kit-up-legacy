
namespace UpLang.Parsing.Evaluations {
	/// <summary>
	///
	/// </summary>
	public abstract class Expression {
		protected string SourceText { get; set; }
		public abstract string Value { get; }

		public static implicit operator string(Expression expr) {
			if ((object)expr != null) {
				return expr.Value;
			}
			else {
				return null;
			}
		}
		public override string ToString() {
			return Value;
		}
	}
}