
namespace UpLang.Parsing.Evaluations {
	public sealed class Atom : Expression {
		public Atom(string text) {
			this.SourceText = text;
		}
		public override string Value {
			get { return SourceText; }
		}

		public static explicit operator Atom(string text) {
			return new Atom(text);
		}

		public static Atom ToAtom(string text) {
			return new Atom(text);
		}
	}
}