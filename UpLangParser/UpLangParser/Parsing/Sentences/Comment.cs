using System;
using System.Linq;

namespace UpLang.Parsing.Sentences {
	/// <summary>
	///
	/// </summary>
	public sealed class Comment : Sentence {
		public string Text { get; private set; }
		public Comment(string line) {
			if (line == null) throw new ArgumentNullException();
			int i = 0;
			while (i < line.Length && line[i] == Parser.CommentStartChar) { i++; }
			while (i < line.Length && Parser.WhiteSpaceChars.Contains(line[i])) i++;
			this.Text = line.Substring(i);
		}
	}
}