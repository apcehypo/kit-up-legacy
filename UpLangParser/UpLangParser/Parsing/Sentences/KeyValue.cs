using System;
using System.Text;
using UpLang.Parsing.Texts;

namespace UpLang.Parsing.Sentences {
  /// <summary>
  ///
  /// </summary>
  public sealed class KeyValue : Sentence {
		public Text Key { get; private set; }
		public Text Value { get; private set; }
		public bool HasAssign { get; private set; }

		private enum LogicalState { Key, Value }
		private enum PhysicalState { Normal, AfterEscape, InQuotes, InSubstitution }
		public KeyValue(string line) {
			if (line == null) throw new ArgumentNullException();
			this.Key = new EmptyText();
			this.Value = new EmptyText();
			LogicalState logicalState = LogicalState.Key;
			PhysicalState physicalState = PhysicalState.Normal;
			PhysicalState previousPhysicalState = PhysicalState.Normal;
			StringBuilder sb = new StringBuilder();
			Text target = new EmptyText();
			for (int i = 0; i < line.Length; i++) {
				switch (physicalState) {
					case PhysicalState.Normal:
						switch (line[i]) {
							case Parser.QuoteChar:
								physicalState = PhysicalState.InQuotes;
								if (sb.Length > 0) {
                  target += (RawText)sb.ToString();//.TrimEnd(Parser.WhiteSpaceChars);
									sb.Clear();
								}
								break;
							case Parser.SubstitutionChar:
								physicalState = PhysicalState.InSubstitution;
								if (sb.Length > 0) {
                  target += (RawText)sb.ToString();//.TrimEnd(Parser.WhiteSpaceChars);
									sb.Clear();
								}
								break;
							case Parser.EscapeChar:
								physicalState = PhysicalState.AfterEscape;
								if (sb.Length > 0) {
									target += (RawText)sb.ToString();
									sb.Clear();
								}
								break;
							default:
								if (logicalState == LogicalState.Key && line[i] == Parser.AssignChar) {
									logicalState = LogicalState.Value;
									if (sb.Length > 0) {
										target += (RawText)sb.ToString().TrimEnd(Parser.WhiteSpaceChars);
										sb.Clear();
									}
									this.Key = target;
									target = new EmptyText();
									this.HasAssign = true;
									line.SkipNextWhiteSpaces(ref i);
								}
								else {
									sb.Append(line[i]);
								}
								break;
						}
						break;
					case PhysicalState.AfterEscape:
						physicalState = previousPhysicalState;
						target += (RawText)line.GetEscapedChar(i);
						break;

					case PhysicalState.InQuotes:
						switch (line[i]) {
							case Parser.QuoteChar:
								physicalState = PhysicalState.Normal;
								target += (RawText)sb.ToString();
								sb.Clear();
								break;
							case Parser.EscapeChar: 
								previousPhysicalState = PhysicalState.InQuotes;
								physicalState = PhysicalState.AfterEscape;
								if (sb.Length > 0) {
									target += (RawText)sb.ToString();
									sb.Clear();
								}
								break;
							default:
								sb.Append(line[i]);
								break;
						}
						break;
					case PhysicalState.InSubstitution:
						switch (line[i]) {
							case Parser.SubstitutionChar:
								physicalState = PhysicalState.Normal;
								target += (EvalText)sb.ToString();                
								sb.Clear();
								break;
							case Parser.EscapeChar: 
								previousPhysicalState = PhysicalState.InSubstitution;
								physicalState = PhysicalState.AfterEscape;
								if (sb.Length > 0) {
									target += (RawText)sb.ToString();
									sb.Clear();
								}
								break;
							default:
								sb.Append(line[i]);
								break;
						}
						break;
					default:
						throw new NotImplementedException("ENUM PhysicalState");
				}
			}
			//строка кончилась
			switch (physicalState) {
				case PhysicalState.Normal:
					switch (logicalState) {
						case LogicalState.Key:
							if (sb.Length > 0) {
								target += (RawText)sb.ToString().TrimEnd(Parser.WhiteSpaceChars);
							}
							this.Key = target;
							this.Value = null;
							break;
						case LogicalState.Value:
							if (sb.Length > 0) {
								target += (RawText)sb.ToString();
							}
							this.Value = target;
							break;
						default:
							throw new NotImplementedException("ENUM LogicalState");
					}
					break;
				case PhysicalState.AfterEscape:
					//ошибка
					break;
				case PhysicalState.InQuotes:
					//ошибка
					break;
				case PhysicalState.InSubstitution:
					//ошибка
					break;
				default:
					throw new NotImplementedException("ENUM PhysicalState");
			}
		}
	}
}