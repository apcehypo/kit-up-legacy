using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using UpLang.Parsing.Texts;

namespace UpLang.Parsing.Sentences {
  /// <summary>
  ///
  /// </summary>
  public sealed class Header : Sentence {
		public Text Title { get; private set; }
		public ReadOnlyCollection<Text> Param { get; private set; }
		public Text Alias { get; private set; }
		public bool HasAlias { get; private set; }

		private enum LogicalState { Header, Param, Alias }
		private enum PhysicalState { Normal, AfterEscape, InQuotes, InSubstitution, AliasAssign }
		public Header(string line) {
			if (line == null) throw new ArgumentNullException();
			List<Text> param = new List<Text>();
			LogicalState logicalState = LogicalState.Header;
			PhysicalState physicalState = PhysicalState.Normal;
			PhysicalState previousPhysicalState = PhysicalState.Normal;
			StringBuilder sb = new StringBuilder();
			Text target = new EmptyText();
			//пропускаем 0-ой символ, он равен '['
			for (int i = 1; i < line.Length; i++) {
				switch (physicalState) {
					case PhysicalState.Normal:
						switch (line[i]) {
							case Parser.SplitterChar:
								physicalState = PhysicalState.Normal;
								if (sb.Length > 0) {
									target += (RawText)sb.ToString().TrimEnd(Parser.WhiteSpaceChars);
									sb.Clear();
								}
								line.SkipNextWhiteSpaces(ref i);
								switch (logicalState) {
									case LogicalState.Header:
										logicalState = LogicalState.Param;
										this.Title = target;
										target = new EmptyText();
										break;
									case LogicalState.Param: //остаётся Param
										param.Add(target);
										target = new EmptyText();
										break;
									case LogicalState.Alias: //остаётся Alias
										sb.Append(line[i]); //пока позволяем иметь символ '|' в псевдонимах
										break;
									default:
										throw new NotImplementedException("ENUM LogicalState");
								}
								break;
							case Parser.HeaderEndChar:
								if (sb.Length > 0) {
									target += (RawText)sb.ToString().TrimEnd(Parser.WhiteSpaceChars);
									sb.Clear();
								}
								switch (logicalState) {
									case LogicalState.Header:
										this.Title = target;
										break;
									case LogicalState.Param:
										param.Add(target);
										break;
									case LogicalState.Alias:
										//ошибка
										break;
									default:
										throw new NotImplementedException("ENUM LogicalState");
								}
								logicalState = LogicalState.Alias;
								physicalState = PhysicalState.AliasAssign;
								line.SkipNextWhiteSpaces(ref i);
								break;
							case Parser.QuoteChar:
								physicalState = PhysicalState.InQuotes;
								if (sb.Length > 0) {
									target += (RawText)sb.ToString();
									sb.Clear();
								}
								break;
							case Parser.SubstitutionChar:
								physicalState = PhysicalState.InSubstitution;
								if (sb.Length > 0) {
									target += (RawText)sb.ToString();
									sb.Clear();
								}
								break;
							case Parser.EscapeChar:
								previousPhysicalState = physicalState;
								physicalState = PhysicalState.AfterEscape;
								if (sb.Length > 0) {
									target += (RawText)sb.ToString();
									sb.Clear();
								}
								break;
							default:
								sb.Append(line[i]);
								break;
						}
						break;
					case PhysicalState.AfterEscape:
						physicalState = previousPhysicalState;
						target += (RawText)line.GetEscapedChar(i);
						break;
					case PhysicalState.InQuotes:
						switch (line[i]) {
							case Parser.QuoteChar:
								physicalState = PhysicalState.Normal;
								if (sb.Length > 0) {
									target += (RawText)sb.ToString();
									sb.Clear();
								}
								break;
							case Parser.EscapeChar:
								previousPhysicalState = physicalState;
								physicalState = PhysicalState.AfterEscape;
								if (sb.Length > 0) {
									target += (RawText)sb.ToString();
									sb.Clear();
								}
								break;
							default:
								sb.Append(line[i]);
								break;
						}
						break;
					case PhysicalState.InSubstitution:
						switch (line[i]) {
							case Parser.SubstitutionChar:
								physicalState = PhysicalState.Normal;
								if (sb.Length > 0) {
									target += (EvalText)sb.ToString();
									sb.Clear();
								}
								break;
							case Parser.EscapeChar:
								previousPhysicalState = physicalState;
								physicalState = PhysicalState.AfterEscape;
								if (sb.Length > 0) {
									target += (RawText)sb.ToString();
									sb.Clear();
								}
								break;
							default:
								sb.Append(line[i]);
								break;
						}
						break;
					case PhysicalState.AliasAssign:
						if (line[i] == Parser.AssignChar) {
							physicalState = PhysicalState.Normal;              
							target = new EmptyText();
							this.HasAlias = false; //если будет не пусто, станет true
						}
						else {
							//ошибка [...]|...
							this.Alias = null;
							i = line.Length; //закончить разбор
						}
						break;
					default:
						throw new NotImplementedException("ENUM PhysicalState");
				}
			}
			//строка кончилась
			this.Param = new ReadOnlyCollection<Text>(param);
			switch (physicalState) {
				case PhysicalState.Normal:
					switch (logicalState) {
						case LogicalState.Header:
							//ошибка
							break;
						case LogicalState.Param:
							//ошибка
							break;
						case LogicalState.Alias:
							if (sb.Length > 0) {
								target += (RawText)sb.ToString();
							}
							this.Alias = target;
							this.HasAlias = true;
							break;
						default:
							throw new NotImplementedException("ENUM LogicalState");
					}
					break;
				case PhysicalState.AliasAssign:

					break;
				case PhysicalState.AfterEscape:
					//ошибка
					break;
				case PhysicalState.InQuotes:
					//ошибка
					break;
				case PhysicalState.InSubstitution:
					//ошибка
					break;
				default:
					throw new NotImplementedException("ENUM PhysicalState");
			}

		}
	}
}