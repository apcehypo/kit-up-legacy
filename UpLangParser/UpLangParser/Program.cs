﻿using System;
using System.IO;
using UpLang.Execution;
using UpLang.Modules;

namespace UpLang {
  class Program {
    /// <summary>
    /// Читает up-файл и исполняет
    /// </summary>
    /// <param name="fileName"></param>
    public static void ExecuteFile(string fileName) {
      var lines = File.ReadAllLines(fileName);
    }

    public static ModuleRepository Repository = new ModuleRepository();
    public static Executor Executor = new Executor();

    static void Main() {
      foreach (var lib in Repository.Modules) {
        Console.ForegroundColor = ConsoleColor.DarkGreen;
        Console.WriteLine($"\r\nБиблиотека: {lib.Metadata.Name} v{lib.Value.Version} от {lib.Value.Copyright}");
        Console.ResetColor();
        foreach (var func in lib.Value.FunctionsImports) {
          Console.ForegroundColor = ConsoleColor.DarkBlue;
          Console.WriteLine($" функция: {func.Metadata.Name} Memoizable:{func.Value.Memoizable} MinArgs:{func.Value.MinimalArity} MaxArgs:{func.Value.MaximalArity}");
          Console.ResetColor();
        }
        foreach (var stat in lib.Value.StatementsImports) {
          Console.ForegroundColor = ConsoleColor.DarkMagenta;
          Console.WriteLine($" оператор: {stat.Metadata.Name}");
          Console.ResetColor();
        }
      }
      /////////////////////////
      string code1 = @"
[let]
key1=value1
key2=`key1`
key3=`key2`
key4=value4
";
      Scope scope = new Scope(null);
      Executor.Execute(code1);

      string code2 = @"
[let]
key4=`key4`
key5=`key4`
key6=`unknown`
key7=`(sampleLib.sampleFunc)`
";
      Executor.Execute(code2);
      //ExecuteFile("test.up");
      /////////////////////////////////
      Console.ReadKey();
    }
  }
}
