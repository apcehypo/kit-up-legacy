﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Win32;

namespace kITUPTester
{
    class RegistryTools
    {
        static string[] ROOTS = new string[] {
            "HKEY_CLASSES_ROOT", "HKCR",
            "HKEY_CURRENT_USER", "HKCU",
            "HKEY_LOCAL_MACHINE", "HKLM",
            "HKEY_USERS", "HKU",
            "HKEY_CURRENT_CONFIG", "HKCC",
            "HKEY_PERFORMANCE_DATA", "HKPD"
         };


        public class PresetSection
        {
            public char Operation;
            public string Key;
            public string SecondKey;
            public bool IsKeyDefined;
            public bool IsSecondKeyDefined;
            public List<PresetValue> ValueList;
        }
        public class PresetValue
        {
            public char Operation;
            public string Key;
            public bool DefParam;
            public string Param;
            public Microsoft.Win32.RegistryValueKind ParamKind;
            public object Value;
            public bool ForceDelete;
        }

        static List<PresetSection> SectionList;

        static Regex S_NAME = new Regex("^(\\[r)(.)(\\|)(.*?)(\\])$");
        static Regex S_NAME_DOUBLE = new Regex("^(\\[r)(.)(\\|)(.*?)(\\|)(.*?)(\\])$");
        static Regex P_VAL = new Regex("^(.*?)(\")(.*?)\"=(.*?)$");
        static Regex P_VAL_DEF = new Regex("^(.*?)@=(.*?)$");

        //For defined values
        static Regex V_STRING = new Regex("^(.*?)\"(.*?)\"=\"(.*?)\"$");
        static Regex V_DWORD = new Regex("^(.*?)\"(.*?)\"=dword:(\\d+)$");
        static Regex V_HEX = new Regex("^(.*?)\"(.*?)\"=hex:(.*?)$");
        static Regex V_HEX_EXT = new Regex("^(.*?)\"(.*?)\"=hex\\((.*?)\\):(.*?)$");

        //For default values
        static Regex V_STRING_DEF = new Regex("^(.*?)@=\"(.*?)\"$");
        static Regex V_DWORD_DEF = new Regex("^(.*?)@=dword:(\\d+)$");
        static Regex V_HEX_DEF = new Regex("^(.*?)@=hex:(.*?)$");
        static Regex V_HEX_EXT_DEF = new Regex("^(.*?)@=hex\\((.*?)\\):(.*?)$");

        /// ------------------------------------------------------------------------------------
        /// <summary> Парсит пресет из указанного файла </summary>
        /// <param name="f_path">Полный путь до пресети</param>
        /// <returns> Список классов PresetValue, т.е. затронутых параметров реестра  </returns>
        /// ------------------------------------------------------------------------------------
        public static List<PresetSection> ParsePreset(string f_path)
        {
            if (!File.Exists(f_path))
                return null;
            string[] f_content = File.ReadAllLines(f_path);

            SectionList = new List<PresetSection>();
            char operation = 'm';

            foreach (string line in f_content)
            {
                if (line.Length == 0)
                    continue;
                if (line[0] == ';')
                    continue;

                bool IsParsed = false;
                PresetValue p = null;


                if (S_NAME_DOUBLE.IsMatch(line))
                {
                    Match m = S_NAME_DOUBLE.Match(line);
                    PresetSection section = new PresetSection();
                    section.ValueList = new List<PresetValue>();
                    section.Operation = m.Groups[2].Value[0];
                    section.Key = m.Groups[4].Value;
                    section.SecondKey = m.Groups[6].Value;
                    section.IsKeyDefined = section.Key.Length != 0;
                    section.IsSecondKeyDefined = section.SecondKey.Length != 0;
                    SectionList.Add(section);
                }
                else if (S_NAME.IsMatch(line))
                {
                    Match m = S_NAME.Match(line);
                    PresetSection section = new PresetSection();
                    section.ValueList = new List<PresetValue>();
                    section.Operation = m.Groups[2].Value[0];
                    section.Key = m.Groups[4].Value;
                    section.IsKeyDefined = section.Key.Length != 0;
                    SectionList.Add(section);
                }
                else if (SectionList.Count == 0)
                    continue;

                if (!IsParsed)
                {
                    p = p_DefinedParam(line);
                    if (p != null)
                    {
                        IsParsed = true;
                        p.Operation = operation;
                    }
                }

                if (!IsParsed)
                {
                    p = p_DefaultParam(line);
                    if (p != null)
                    {
                        IsParsed = true;
                        p.Operation = operation;
                    }
                }

                if (IsParsed && p != null)
                {
                    var oldvalue = -1;
                    if (p.DefParam)
                        oldvalue = SectionList[SectionList.Count - 1].ValueList.FindIndex(i => i.DefParam == true && i.Key == p.Key);
                    else
                        oldvalue = SectionList[SectionList.Count - 1].ValueList.FindIndex(i => i.Param == p.Param && i.Key == p.Key);
                    if (oldvalue != -1)
                        SectionList[SectionList.Count - 1].ValueList.RemoveAt(oldvalue);
                    if (!p.ForceDelete)
                        SectionList[SectionList.Count - 1].ValueList.Add(p);
                }
            }

            return SectionList;
        }

        /// ------------------------------------------------------------------------------------
        /// <summary> Парсит строку определенного параметра реестра (не Default) </summary>
        /// <param name="line">Строка "[путь]параметр=значение"</param>
        /// <returns> Элемент PresetValue с информацией занного параметра </returns>
        /// ------------------------------------------------------------------------------------
        private static PresetValue p_DefinedParam(string line)
        {
            PresetValue p = new PresetValue();
            Match m = null;
            if (V_STRING.IsMatch(line))
            {
                m = V_STRING.Match(line);
                p.ParamKind = RegistryValueKind.String;
                p.Param = m.Groups[2].Value;
                p.Value = m.Groups[3].Value;
            }
            else if (V_DWORD.IsMatch(line))
            {
                m = V_DWORD.Match(line);
                p.ParamKind = RegistryValueKind.DWord;
                p.Param = m.Groups[2].Value;
                p.Value = Convert.ToInt32(m.Groups[3].Value.Trim(new char[] { '0' }));
            }
            else if (V_HEX.IsMatch(line))
            {
                m = V_HEX.Match(line);
                p.ParamKind = RegistryValueKind.Binary;
                p.Param = m.Groups[2].Value;
                if (!CheckHexSeq(m.Groups[3].Value))
                    return null;
                p.Value = m.Groups[3].Value.Split(',').Select(b => Convert.ToByte(b, 16)).ToArray();
            }
            else if (V_HEX_EXT.IsMatch(line))
            {
                m = V_HEX_EXT.Match(line);
                p.Param = m.Groups[2].Value;
                if (!CheckHexSeq(m.Groups[4].Value))
                    return null;
                HexSwitch(ref p, m.Groups[3].Value, m.Groups[4].Value);
            }
            else
                return null;

            if (!SectionList[SectionList.Count - 1].IsKeyDefined)
            {
                if (m.Groups[1].Value.Length == 0)
                    return null;
                p.Key = m.Groups[1].Value.Substring(0, m.Groups[1].Value.Length - 1);
            }

            return p;
        }

        /// ------------------------------------------------------------------------------------
        /// <summary> Парсит строку стандартного параметра реестра </summary>
        /// <param name="line">Строка "[путь]параметр=значение"</param>
        /// <returns> Элемент PresetValue с информацией занного параметра </returns>
        /// ------------------------------------------------------------------------------------
        private static PresetValue p_DefaultParam(string line)
        {
            PresetValue p = new PresetValue();
            Match m = null;
            if (V_STRING_DEF.IsMatch(line))
            {
                m = V_STRING_DEF.Match(line);
                p.ParamKind = RegistryValueKind.String;
                p.DefParam = true;
                p.Value = m.Groups[2].Value;
            }
            else if (V_DWORD_DEF.IsMatch(line))
            {
                m = V_DWORD_DEF.Match(line);
                p.ParamKind = RegistryValueKind.DWord;
                p.DefParam = true;
                p.Value = Convert.ToInt32(m.Groups[2].Value.Trim(new char[] { '0' }));
            }
            else if (V_HEX_DEF.IsMatch(line))
            {
                m = V_HEX_DEF.Match(line);
                p.ParamKind = RegistryValueKind.Binary;
                p.DefParam = true;
                if (!CheckHexSeq(m.Groups[2].Value))
                    return null;
                p.Value = m.Groups[2].Value.Split(',').Select(b => Convert.ToByte(b, 16)).ToArray();
            }
            else if (V_HEX_EXT_DEF.IsMatch(line))
            {
                m = V_HEX_EXT_DEF.Match(line);
                p.DefParam = true;
                HexSwitch(ref p, m.Groups[2].Value, m.Groups[3].Value);
            }
            else
                return null;

            if (!SectionList[SectionList.Count - 1].IsKeyDefined)
            {
                if (m.Groups[1].Value.Length == 0)
                    return null;
                p.Key = m.Groups[1].Value.Substring(0, m.Groups[1].Value.Length - 1);
            }

            return p;
        }

        /// ------------------------------------------------------------------------------------
        /// <summary> Конвертация 16-ричного параметра в соответствии с типом
        /// и заполнение его данных в переданный PresetValue</summary>
        /// <param name="p">Ссылка на PresetValue</param>
        /// <param name="type">Тип параметра (7 - REG_MULTI_SZ и т.п.)</param>
        /// <param name="data">16-ричные данные в формате "<hex-байт>[,<hex-байт>]" </param>
        /// ------------------------------------------------------------------------------------
        private static void HexSwitch(ref PresetValue p, string type, string data)
        {
            switch (type)
            {
                case "0"://REG_NONE
                case "5":
                    {
                        p.ParamKind = RegistryValueKind.None;
                        p.Value = data.Split(',').Select(b => Convert.ToByte(b, 16)).ToArray();
                        break;
                    }
                case "2"://REG_EXPAND_SZ
                    {
                        p.ParamKind = RegistryValueKind.ExpandString;
                        byte[] bytes = data.Split(',').Select(b => Convert.ToByte(b, 16)).ToArray();
                        string str = Encoding.Unicode.GetString(bytes);
                        if (str[str.Length - 1] == '\0')
                            str = str.Substring(0, str.Length - 1);
                        p.Value = Environment.ExpandEnvironmentVariables(str);
                        break;
                    }
                case "7"://REG_MULTI_SZ
                    {
                        p.ParamKind = RegistryValueKind.MultiString;
                        byte[] bytes = data.Split(',').Select(b => Convert.ToByte(b, 16)).ToArray();
                        string str = Encoding.Unicode.GetString(bytes).Replace("\0\0", string.Empty);
                        p.Value = str.Split('\0');
                        break;
                    }
                case "b"://REG_QWORD
                    {
                        p.ParamKind = RegistryValueKind.QWord;
                        p.Value = BitConverter.ToInt64(data.Split(',').Select(b => Convert.ToByte(b, 16)).ToArray(), 0);
                        break;
                    }

                case "6"://REG_LINK
                case "8"://REG_RESOURCE_LIST
                case "9"://REG_FULL_RESOURCE_DESCRIPTOR
                case "a"://REG_RESOURCE_REQUIREMENTS_LIST
                    {
                        //Console.Write("REG_LINK and resource types aren't supported. Ignoring... ");
                        p.ForceDelete = true;
                        break;
                    }
            }
        }

        /// ------------------------------------------------------------------------------------
        /// <summary> Проверка валидности последовательности байт вформате 
        /// "<hex-байт>[,<hex-байт>]" </summary>
        /// <param name="hex">16-ричные данные в формате "<hex-байт>[,<hex-байт>]" </param>
        /// <returns> Возвращает <see langword="true"/>, если проверка прошла успешно </returns>
        /// ------------------------------------------------------------------------------------
        private static bool CheckHexSeq(string hex)
        {
            return new Regex("^([A-Fa-f0-9]{2}[,]\\s?)+$").IsMatch(hex + ",");
        }

        /// ------------------------------------------------------------------------------------
        /// <summary> Получение списка затронутых пресетом ключей </summary>
        /// <param name="SectionList">Список секций пресета </param>
        /// <returns> Возвращает список строк - затронутых ключей </returns>
        /// ------------------------------------------------------------------------------------
        public static List<string> GetKeyList(List<PresetSection> SectionList)
        {
            List<string> KeyList = new List<string>();

            foreach (PresetSection Section in SectionList)
            {
                if (Section.IsKeyDefined)
                {
                    string Key = Section.Key;
                    if (KeyList.FindIndex(i => i == Key) == -1)
                        KeyList.Add(Key);

                    if (Section.IsSecondKeyDefined)
                    {
                        bool RootFound = false;
                        foreach(string root in ROOTS)
                            RootFound = RootFound || Section.SecondKey.IndexOf(root) == 0;

                        if (RootFound)
                            KeyList.Add(Section.SecondKey);
                        else
                            KeyList.Add(Key.Substring(0, Key.LastIndexOf('\\') + 1) + Section.SecondKey);
                    }
                    continue;
                }

                foreach (PresetValue Value in Section.ValueList)
                    if (KeyList.FindIndex(i => i == Value.Key) == -1)
                        KeyList.Add(Value.Key);
            }

            return KeyList;
        }

        #region Registry operations

        /// ------------------------------------------------------------------------------------
        /// <summary> Удаляет указанные в списке ключи </summary>
        /// <param name="Keys"> Список ключей для удаления </param>
        /// ------------------------------------------------------------------------------------
        public static void DeleteKeys(List<string> Keys)
        {
            foreach (string Key in Keys)
                DeleteKey(Key);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary> Удаляет указанный ключ </summary>
        /// <param name="registryPath"> Путь ключа для удаления </param>
        /// ------------------------------------------------------------------------------------
        public static void DeleteKey(string registryPath)
        {
            var process = new System.Diagnostics.Process();

            try
            {
                process.StartInfo.FileName = "reg.exe";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.Arguments = string.Format("DELETE \"{0}\" /f", registryPath);
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.Start();

                process.WaitForExit();
            }
            catch (Exception)
            {
                process.Dispose();
            }
        }

        /// ------------------------------------------------------------------------------------
        /// <summary> Экспорт ключа реестра в файл </summary>
        /// <param name="exportPath"> Путь до файла, куда необходимо экспортировать </param>
        /// <param name="registryPath"> Экспортируемый ключ </param>
        /// <returns> Возвращает строку с ошибкой в процессе экспортирования </returns>
        /// ------------------------------------------------------------------------------------
        public static string ExportKey(string exportPath, string registryPath)
        {
            var process = new System.Diagnostics.Process();
            try
            {
                process.StartInfo.FileName = "reg.exe";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.Arguments = string.Format("EXPORT \"{0}\" \"{1}\"", registryPath, exportPath);
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.Start();
                process.WaitForExit();
                return process.StandardError.ReadToEnd();
            }
            catch (Exception ex)
            {
                process.Dispose();
                return ex.Message;
            }
        }

        /// ------------------------------------------------------------------------------------
        /// <summary> Получение дампа реестра из нескольких веток </summary>
        /// <param name="Notes"> Ссылка на список с заметками теста </param>
        /// <param name="Keys"> Ключи, которые необходимо экспортировать в файл </param>
        /// <returns> Возвращает строку с путем до файла дампа </returns>
        /// ------------------------------------------------------------------------------------
        public static string GetDump(ref List<Note> Notes, List<string> Keys)
        {
            string RegHeader = "Windows Registry Editor Version 5.00";
            string DumpPath = Environment.GetEnvironmentVariable("temp") + "\\kITUPTester_full.reg";
            string ExportPath = Environment.GetEnvironmentVariable("temp") + "\\kITUPTester_export.reg";
            string tmp;

            try { File.Delete(DumpPath); }
            catch { }

            using (StreamWriter sw = File.CreateText(DumpPath))
            {
                sw.WriteLine(RegHeader);

                foreach (string Key in Keys)
                {
                    try { File.Delete(ExportPath); }
                    catch { }
                    string result = RegistryTools.ExportKey(ExportPath, Key).Trim();
                    if (result.Length > 0)
                    {
                        List<string> Data = new List<string>();
                        Data.Add(result);
                        Notes.Add(new Note() { Type = NoteType.None, Kind = NoteKind.reg_ExportNote, Data = Data });
                    }

                    if (File.Exists(ExportPath))
                    {
                        using (StreamReader sr = File.OpenText(ExportPath))
                        {
                            while (!sr.EndOfStream)
                            {
                                tmp = sr.ReadLine();
                                if (tmp != RegHeader)
                                    sw.WriteLine(tmp);
                            }
                        }
                    }
                }
            }

            return DumpPath;
        }

        #endregion

        #region Comparing

        /// ------------------------------------------------------------------------------------
        /// <summary> Подготавливает список строк файла реестра для сравнения. Сворачивает 
        /// hex-значения и сортирует список по алфавиту. </summary>
        /// <param name="lines"> Список строк файла реестра </param>
        /// <returns> Обработанный список строк файла реестра </returns>
        /// ------------------------------------------------------------------------------------
        public static List<string> PrepareList(List<string> lines)
        {
            //Сворачивание "hex"-значений
            List<string> new_lines = new List<string>();
            bool IsCollapse = false;
            string newline = string.Empty;
            foreach (string line in lines)
            {
                if (IsCollapse)
                {
                    newline += line.Trim();
                    IsCollapse = false;
                }
                else
                    newline = line.Trim();

                if (newline.Length > 2)
                {
                    if (newline.Substring(newline.Length - 2) == ",\\")
                    {
                        newline = newline.Substring(0, newline.Length - 1);
                        IsCollapse = true;
                        continue;
                    }
                }
                new_lines.Add(newline);
            }

            //Сортировка списка по алфавиту.
            new_lines.Sort();
            return new_lines;
        }

        #region Not Realized
        /*
        public static bool CompareValues(List<RegistryTools.PresetValue> ValueList)
        {
            foreach (RegistryTools.PresetValue Value in ValueList)
            {
                switch (Value.Operation)
                {
                    case 'm':
                        {
                            if (!cmp_m(Value))
                                return false;
                            break;
                        }
                    default:
                        {
                            throw new NotImplementedException();
                        }
                }
            }
            return true;
        }

        protected static bool cmp_m(RegistryTools.PresetValue Value)
        {
            object val = Registry.GetValue(Value.Key, Value.DefParam ? null : Value.Param, null);
            if (val == null)
                return false;

            //BYTE ARRAY:
            //REG_BINARY, REG_DWORD_BIG_ENDIAN, REG_NONE

            //NOT SUPPORTED:
            //REG_LINK, REG_RESOURCE_LIST, REG_FULL_RESOURCE_DESCRIPTOR, REG_RESOURCE_REQUIREMENTS_LIST

            switch (Value.ParamKind)
            {
                case RegistryValueKind.DWord:
                    {
                        if ((int)val != (int)Value.Value)
                            return false;
                        break;
                    }
                case RegistryValueKind.ExpandString:
                case RegistryValueKind.String:
                    {
                        if ((string)val != (string)Value.Value)
                            return false;
                        break;
                    }
                case RegistryValueKind.MultiString:
                    {
                        if (!((string[])Value.Value).SequenceEqual<string>((string[])val))
                            return false;
                        break;
                    }
                case RegistryValueKind.QWord:
                    {
                        if ((long)val != (long)Value.Value)
                            return false;
                        break;
                    }
                default: //REG_BINARY, REG_DWORD_BIG_ENDIAN, REG_NONE
                    {
                        if (!((byte[])Value.Value).SequenceEqual<byte>((byte[])val))
                            return false;
                        break;
                    }
            }

            return true;
        }

        */
        #endregion

        #endregion
    }
}
