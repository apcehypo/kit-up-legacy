﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;

namespace kITUPTester
{
    class INITest : UPTest
    {
        static string SOURCE_INI = "Source.ini";
        static string TARGET_INI = "Target.ini";
        static string CHECK_INI = "Check.ini";
        static string RESULT_INI = "LastResult.ini";

        public INITest(string UP_PATH, string TESTS_DIR) 
            : base(UP_PATH, TESTS_DIR)
        {
            TestName = "Ini";
        }

        public override void DoTest(string test, string test_name)
        {
            Console.Write(" {0}: ", test_name);

            bool CheckPreset = false; //требуется ли "самопроверка" пресета

            string target = Path.Combine(UP_DIR, TARGET_INI);
            string source = Path.Combine(test, SOURCE_INI);
            string check = Path.Combine(test, CHECK_INI);
            string preset = Path.Combine(test, PRESET_NAME);
            string cpreset = Path.Combine(test, CPRESET_NAME);
            string spreset = Path.Combine(test, SPRESET_NAME);
            string resultfile = Path.Combine(test, RESULT_INI);

            //Если найдены CheckPreset & SourcePreset, тогда требуется самопроверка пресета
            if (File.Exists(cpreset) && File.Exists(spreset))
            {
                CheckPreset = true;
                try { File.Copy(spreset, preset, true); }
                catch { }
            }

            //Проверяем наличие требуемых файлов
            if (!File.Exists(source) || !File.Exists(check) || !File.Exists(preset))
            {
                WriteLineColored("WRONG TEST!", ConsoleColor.Red);
                Notes.Add(new Note() { Type = NoteType.Error, Kind = NoteKind.cm_WrongTest });
                return;
            }

            //Корипуем таргет-файл в папку с kitUP
            try { File.Copy(source, target, true); }
            catch
            {
                WriteLineColored("CAN'T COPY SOURCE FILE!", ConsoleColor.Red);
                Notes.Add(new Note() { Type = NoteType.Error, Kind = NoteKind.ini_CantCopySource });
                return;
            }

            //Применяем пресет
            kITUP_proc.StartInfo.Arguments = string.Format("/A \"{0}\"", preset);
            kITUP_proc.Start();
            kITUP_proc.WaitForExit();

            try
            {
                File.Delete(resultfile);
                File.Copy(target, resultfile);
            }
            catch { }

            //Сравниваем таргет-файл с проверочным
            bool IsSuccess = CompareFiles(ref Notes, target, check, null);
            if (CheckPreset)
            {
                //Сравниваем пресет с проверочным
                bool IsSuccessPreset = CompareFiles(ref Notes, cpreset, preset, null);
                IsSuccess = IsSuccess && IsSuccessPreset;
            }
            WriteResult(IsSuccess);

            //Если тест потерпел неудачу, записываем его имя в список проваленных тестов
            if (!IsSuccess)
                Notes.Add(new Note() { Type = NoteType.Error, Kind = NoteKind.cm_FailedTest, Data = null });

            //Удаляем все временные файлы
            try {
              if (CheckPreset)
                File.Delete(preset);
              File.Delete(target);
            }
            catch { }
        }
    }
}