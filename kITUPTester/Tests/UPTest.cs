﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace kITUPTester
{
    class UPTest
    {
        protected static string CPRESET_NAME = "CheckPreset.up";
        protected static string SPRESET_NAME = "SourcePreset.up";
        protected static string PRESET_NAME = "Preset.up";
        protected string TESTS_DIR;
        protected string TestName;
        protected List<Note> Notes;
        protected static string UP_DIR;
        protected static Process kITUP_proc = null;
        public virtual void DoTest(string test, string test_name) { }

        public UPTest(string UP_PATH, string TESTS_DIR)
        {
            UP_DIR = Path.GetDirectoryName(UP_PATH);
            this.TESTS_DIR = TESTS_DIR;
            kITUP_proc = new Process();
            kITUP_proc.StartInfo.FileName = UP_PATH;
            kITUP_proc.StartInfo.WorkingDirectory = UP_DIR;
        }

        public virtual void BeginTest()
        {
            Console.WriteLine();
            Console.WriteLine("{0} testing:", TestName);
            TestType tType = new TestType();
            tType.Name = string.Format("{0} tests", TestName);
            tType.Tests = new List<Test>();

            try
            {
                string[] tests = Directory.GetDirectories(TESTS_DIR);
                if (tests.Length > 0)
                    foreach (string test in tests)
                    {
                        Notes = new List<Note>();
                        string test_name = test.Substring(test.LastIndexOf('\\') + 1);
                        if (test_name.Length > 0)
                            if (test_name[0] == '!') //игнорируем тест с пометкой '!' в названии
                                continue;
                        DoTest(test, test_name);
                        tType.Tests.Add(new Test() { Name = test_name, Notes = Notes });
                    }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }

            NotesProvider.Add(tType);
            Console.WriteLine("{0} testing completed.", TestName);
        }

        #region Вывод в консоль
        protected static void WriteLineColored(string text, ConsoleColor color)
        {
            WriteColored(text, color);
            Console.WriteLine();
        }

        protected static void WriteColored(string text, ConsoleColor color)
        {
            ConsoleColor old = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.Write(text);
            Console.ForegroundColor = old;
        }

        protected static void WriteResult(bool success)
        {
            if (!success)
                WriteLineColored("FAILED!", ConsoleColor.Red);
            else
                WriteLineColored("OK!", ConsoleColor.Green);
        }
        #endregion

        protected delegate List<string> PrepareFunc(List<string> lines);
        protected static bool CompareFiles(ref List<Note> Notes, string one_file, string two_file, PrepareFunc PFunc)
        {
            if (!File.Exists(one_file) || !File.Exists(two_file))
                return false;
            List<string> original_first_list = File.ReadAllLines(one_file).ToList<string>().Where(s => s != string.Empty).ToList();
            List<string> original_sec_list = File.ReadAllLines(two_file).ToList<string>().Where(s => s != string.Empty).ToList();

            List<string> first_list, sec_list;
            if (PFunc != null)
            {
                first_list = PFunc(original_first_list);
                sec_list = PFunc(original_sec_list);
            }
            else
            {
                first_list = original_first_list;
                sec_list = original_sec_list;
            }

            if (first_list.Count != sec_list.Count)
                Notes.Add(new Note() { Type = NoteType.Warning, Kind = NoteKind.cm_LinesCountNotEQ });
            for (int i = 0; i < first_list.Count; i++)
                if (first_list[i] != sec_list[i])
                {
                    List<string> Data = new List<string>();
                    Data.Add((PFunc != null ? original_sec_list.FindIndex(val => val == sec_list[i]) + 1 : i + 1).ToString());
                    Data.Add(sec_list[i]);
                    Data.Add(first_list[i]);
                    Notes.Add(new Note() { Type = NoteType.Error, Kind = NoteKind.cm_compareNote, Data = Data });
                    return false;
                }
            return true;
        }
    }
}
