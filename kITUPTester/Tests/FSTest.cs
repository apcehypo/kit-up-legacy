﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;

namespace kITUPTester
{
    class FSTest : UPTest
    {
        static string CHECK_MAP = "Check.xml";
        static string Sandbox_MAP = "Sandbox.xml";
        static string ConditionsFile = "conditions.cfg";

        public FSTest(string UP_PATH, string TESTS_DIR)
            : base(UP_PATH, TESTS_DIR)
        {
            TestName = "File";
        }

        public override void DoTest(string test, string test_name)
        {
            Console.Write(" {0}: ", test_name);
            string check = Path.Combine(test, CHECK_MAP);
            string preset = Path.Combine(test, PRESET_NAME);
            string cond_file = Path.Combine(test, ConditionsFile);
            string sb_map = Path.Combine(test, Sandbox_MAP);

            //Проверяем наличие требуемых файлов
            if (!File.Exists(preset) || !File.Exists(check) || !File.Exists(sb_map))
            {
                WriteLineColored("WRONG TEST!", ConsoleColor.Red);
                Notes.Add(new Note() { Type = NoteType.Error, Kind = NoteKind.cm_WrongTest });
                return;
            }

            UPDirectory sbDir = FSTools.DeSerialize(sb_map);
            if (!GenerateSandbox(sbDir, UPTest.UP_DIR))
            {
                WriteLineColored("CAN'T CREATE SANDBOX!", ConsoleColor.Red);
                Notes.Add(new Note() { Type = NoteType.Error, Kind = NoteKind.fs_sb_cr_error });
                return;
            }

            //Применяем пресет
            kITUP_proc.StartInfo.Arguments = string.Format("/A \"{0}\"", preset);
            kITUP_proc.Start();
            kITUP_proc.WaitForExit();

            UPDirectory sbRes = FSTools.DirectoryInfoToUP(new DirectoryInfo(Path.Combine(UPTest.UP_DIR, sbDir.Name)));
            UPDirectory sbCheck = FSTools.DeSerialize(check);

            bool IsSuccess = sbCheck.Compare(ref Notes, sbRes, ReadConditions(cond_file));
            WriteResult(IsSuccess);

            //Если тест потерпел неудачу, записываем его имя в список проваленных тестов
            if (!IsSuccess)
                Notes.Add(new Note() { Type = NoteType.Error, Kind = NoteKind.cm_FailedTest, Data = null });

            //Удаляем все временные файлы
            try { Directory.Delete(Path.Combine(UPTest.UP_DIR, sbDir.Name), true); }
            catch { }
        }

        private UPDirectory.FSCondition ReadConditions(string file)
        {
            UPDirectory.FSCondition fCond = UPDirectory.FSCondition.None;
            
            if (!File.Exists(file))
                return fCond;

            string[] lines = File.ReadAllLines(file);

            foreach (string line in lines)
            {
                string line_ = line.Trim();
                if (line_.Length == 0)
                    continue;
                if (line_[0] == '#')
                    continue;
                if (line_.Contains("FileAttributes"))
                    fCond |= UPDirectory.FSCondition.FileAttributes;
                if (line_.Contains("FileCreationTime"))
                    fCond |= UPDirectory.FSCondition.FileCreationTime;
                if (line_.Contains("FileLastAccessTime"))
                    fCond |= UPDirectory.FSCondition.FileLastAccessTime;
                if (line_.Contains("FileLastWriteTime"))
                    fCond |= UPDirectory.FSCondition.FileLastWriteTime;
                if (line_.Contains("DirAttributes"))
                    fCond |= UPDirectory.FSCondition.DirAttributes;
                if (line_.Contains("DirCreationTime"))
                    fCond |= UPDirectory.FSCondition.DirCreationTime;
                if (line_.Contains("DirLastAccessTime"))
                    fCond |= UPDirectory.FSCondition.DirLastAccessTime;
                if (line_.Contains("DirLastWriteTime"))
                    fCond |= UPDirectory.FSCondition.DirLastWriteTime;
            }

            return fCond;
        }

        private bool GenerateSandbox(UPDirectory upDir, string folder)
        {
            try { Directory.Delete(Path.Combine(folder, upDir.Name), true); }
            catch { }
            if (upDir != null)
                return FSTools.GenerateFiles(upDir, folder);
            return false;
        }
    }
}
