﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;
using Microsoft.Win32;

namespace kITUPTester
{
    class REGTest : UPTest
    {
        static string CHECK_REG = "Check.reg";
        static string RESULT_REG = "LastResult.reg";

        public REGTest(string UP_PATH, string TESTS_DIR) 
            : base(UP_PATH, TESTS_DIR)
        {
            TestName = "Registry";
        }

        public override void DoTest(string test, string test_name)
        {
            Console.Write(" {0}: ", test_name);
            bool CheckPreset = false; //требуется ли "самопроверка" пресета

            string check = Path.Combine(test, CHECK_REG);
            string preset = Path.Combine(test, PRESET_NAME);
            string cpreset = Path.Combine(test, CPRESET_NAME);
            string spreset = Path.Combine(test, SPRESET_NAME);
            string resultfile = Path.Combine(test, RESULT_REG);

            //Если найдены CheckPreset & SourcePreset, тогда требуется самопроверка пресета
            if (File.Exists(cpreset) && File.Exists(spreset))
            {
                CheckPreset = true;
                try { File.Copy(spreset, preset, true); }
                catch { }
            }

            //Проверяем наличие требуемых файлов
            if (!File.Exists(preset))
            {
                WriteLineColored("WRONG TEST!", ConsoleColor.Red);
                Notes.Add(new Note() { Type = NoteType.Error, Kind = NoteKind.cm_WrongTest });
                return;
            }

            //Парсим пресет, получаем список затронутых ключей
            List<RegistryTools.PresetSection> Sections = RegistryTools.ParsePreset(preset);
            List<string> KeyList = RegistryTools.GetKeyList(Sections);
            RegistryTools.DeleteKeys(KeyList);

            //Применяем пресет
            kITUP_proc.StartInfo.Arguments = string.Format("/A \"{0}\"", preset);
            kITUP_proc.Start();
            kITUP_proc.WaitForExit();

            //Дампим затронутые ключи в один файл
            string ExportFile = RegistryTools.GetDump(ref Notes, KeyList);

            try
            {
                File.Delete(resultfile);
                File.Copy(ExportFile, resultfile);
            }
            catch { }

            //Убираем за собой ;)
            RegistryTools.DeleteKeys(KeyList);

            //Сравниваем сдампенный файл с проверочным
            bool IsSuccess = CompareFiles(ref Notes, check, ExportFile, RegistryTools.PrepareList);
            if (CheckPreset)
            {
                //Сравниваем пресет с проверочным
                bool IsSuccessPreset = CompareFiles(ref Notes, cpreset, preset, null);
                IsSuccess = IsSuccess && IsSuccessPreset;
            }
            WriteResult(IsSuccess);

            if (!IsSuccess)
                Notes.Add(new Note() { Type = NoteType.Error, Kind = NoteKind.cm_FailedTest, Data = null });
        }
    }
}