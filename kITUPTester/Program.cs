﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace kITUPTester
{
    class Program
    {
        static string APP_PATH, kITUP_PATH;
        static string AppInfo;

        static bool CheckUP()
        {
            if (!File.Exists(kITUP_PATH))
                return false;
            return true;
        }

        static void Main(string[] args)
        {
            AppInfo = string.Format("kIT Universal Presets Tester v{0} - GoldRenard 2013", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version);
            Console.Title = AppInfo;
            Console.WriteLine(AppInfo);
            APP_PATH = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            kITUP_PATH = APP_PATH + "\\kITUP.exe";
            if (CheckUP())
            {
                List<UPTest> Tests = new List<UPTest>();
                Tests.Add(new INITest(kITUP_PATH, APP_PATH + "\\INITests"));
                Tests.Add(new REGTest(kITUP_PATH, APP_PATH + "\\REGTests"));
                Tests.Add(new FSTest(kITUP_PATH, APP_PATH + "\\FSTests"));
                while (true)
                {
                    Console.Clear();
                    NotesProvider.Clear();
                    Console.WriteLine(AppInfo);
                    foreach (UPTest Test in Tests)
                        Test.BeginTest();
                    NotesProvider.Write();

                    Console.WriteLine();
                    Console.Write("Press SPACE to repeat testing or any other key to exit...");
                    if (Console.ReadKey().KeyChar == 32)
                        continue;
                    break;
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("kITUP not found!");
            }
        }
    }
}
