﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kITUPTester
{
    public enum NoteKind
    {
        cm_WrongTest,
        cm_FailedTest,
        cm_LinesCountNotEQ,
        cm_compareNote,
        reg_ExportNote,
        ini_CantCopySource,
        fs_sb_cr_error,
        fs_f404,
        fs_d404,
        fs_wrFile,
        fs_atFile,
        fs_ctFile,
        fs_latFile,
        fs_lwtFile,
        fs_atDir,
        fs_ctDir,
        fs_latDir,
        fs_lwtDir,
        fs_wrTime
    }

    public enum NoteType
    {
        None,
        Error,
        Warning
    }


    public struct Note { public NoteType Type; public NoteKind Kind; public List<string> Data; }
    public struct Test { public string Name; public List<Note> Notes; }
    public struct TestType { public string Name; public List<Test> Tests; }

    public class NotesProvider
    {
        static List<TestType> tCollection = new List<TestType>();

        public static void Write()
        {
            if (tCollection.Count == 0)
                return;
            Console.WriteLine();
            Console.WriteLine("Test notes:");

            foreach (TestType tType in tCollection)             // Набор типов тестов
            {
                Console.WriteLine(" {0}:", tType.Name);
                foreach (Test test in tType.Tests)              // Набор тестов
                {
                    if (test.Notes.Count == 0)
                        continue;
                    Console.WriteLine("  {0}:", test.Name);
                    foreach (Note tNote in test.Notes)          // Набор заметок тестов
                    {
                        if (tNote.Kind != NoteKind.cm_FailedTest)
                            Console.WriteLine("   {0}", NoteMsg(tNote));
                    }
                }
            }

        }

        public static string NoteMsg(Note note)
        {
            string result = string.Empty;

            switch (note.Type)
            {
                case NoteType.Error:
                    {
                        result += "Ошибка: ";
                        break;
                    }
                case NoteType.Warning:
                    {
                        result += "Предупреждение: ";
                        break;
                    }
            }

            switch (note.Kind)
            {
                case NoteKind.cm_WrongTest:
                    return result + "Тест не содержит необходимых ресурсов!";
                case NoteKind.cm_LinesCountNotEQ:
                    return result + "Количество строк сравниваемых файлов не равно!";
                case NoteKind.cm_compareNote:
                    return result + string.Format("Строка {0}: Обнаружено \"{1}\", ожидалось \"{2}\"", note.Data[0], note.Data[1], note.Data[2]);

                case NoteKind.reg_ExportNote:
                    return result + note.Data[0];

                case NoteKind.ini_CantCopySource:
                    return result + "Не удается скопировать исходный файл теста!";

                case NoteKind.fs_sb_cr_error:
                    return result + "Не удалось создать песочницу для теста";

                case NoteKind.fs_f404:
                    return result + string.Format("Файл \"{0}\" не найден!", note.Data[0]);
                case NoteKind.fs_d404:
                    return result + string.Format("Папка \"{0}\" не найдена!", note.Data[0]);
                case NoteKind.fs_wrFile:
                    return result + string.Format("Файл \"{0}\" найден, однако отличается размером!", note.Data[0]);

                case NoteKind.fs_atFile:
                    return result + string.Format("Атрибуты файла \"{0}\" отличаются. Требуется \"{1}\", обнаружено \"{2}\"!", note.Data[0], note.Data[1], note.Data[2]);
                case NoteKind.fs_ctFile:
                    return result + string.Format("Даты создания файла \"{0}\" отличаются. Требуется \"{1}\", обнаружено \"{2}\"!", note.Data[0], note.Data[1], note.Data[2]);
                case NoteKind.fs_latFile:
                    return result + string.Format("Даты открытия файла \"{0}\" отличаются. Требуется \"{1}\", обнаружено \"{2}\"!", note.Data[0], note.Data[1], note.Data[2]);
                case NoteKind.fs_lwtFile:
                    return result + string.Format("Даты модификации файла \"{0}\" отличаются. Требуется \"{1}\", обнаружено \"{2}\"!", note.Data[0], note.Data[1], note.Data[2]);

                case NoteKind.fs_atDir:
                    return result + string.Format("Атрибуты папки \"{0}\" отличаются. Требуется \"{1}\", обнаружено \"{2}\"!", note.Data[0], note.Data[1], note.Data[2]);
                case NoteKind.fs_ctDir:
                    return result + string.Format("Даты создания папки \"{0}\" отличаются. Требуется \"{1}\", обнаружено \"{2}\"!", note.Data[0], note.Data[1], note.Data[2]);
                case NoteKind.fs_latDir:
                    return result + string.Format("Даты открытия папки \"{0}\" отличаются. Требуется \"{1}\", обнаружено \"{2}\"!", note.Data[0], note.Data[1], note.Data[2]);
                case NoteKind.fs_lwtDir:
                    return result + string.Format("Даты модификации папки \"{0}\" отличаются. Требуется \"{1}\", обнаружено \"{2}\"!", note.Data[0], note.Data[1], note.Data[2]);

                case NoteKind.fs_wrTime:
                    return result + string.Format("Время модификации файла \"{0}\" отличается!", note.Data[0]);
            }

            return string.Empty;
        }

        public static void Add(TestType tType)
        {
            tCollection.Add(tType);
        }

        public static void Clear()
        {
            tCollection.Clear();
        }
    }
}
