﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;

namespace kITUPTester
{

    public class FSTools
    {
        public static UPFile FileInfoToUP(FileInfo fInfo)
        {
            return new UPFile()
            {
                Name = fInfo.Name,
                Length = fInfo.Length,
                Attributes = fInfo.Attributes,
                CreationTime = fInfo.CreationTime,
                LastAccessTime = fInfo.LastAccessTime,
                LastWriteTime = fInfo.LastWriteTime
            };
        }

        public static UPDirectory DirectoryInfoToUP(DirectoryInfo dInfo)
        {
            UPDirectory upDir = new UPDirectory()
            {
                Name = dInfo.Name,
                Attributes = dInfo.Attributes,
                CreationTime = dInfo.CreationTime,
                LastAccessTime = dInfo.LastAccessTime,
                LastWriteTime = dInfo.LastWriteTime
            };
            upDir.Directories = DirectoryInfoToUP(dInfo.GetDirectories());
            upDir.Files = FileInfoToUP(dInfo.GetFiles());
            return upDir;
        }

        public static List<UPFile> FileInfoToUP(FileInfo[] fInfos)
        {
            List<UPFile> fList = new List<UPFile>();
            foreach (FileInfo fInfo in fInfos)
                fList.Add(FileInfoToUP(fInfo));
            if (fList.Count == 0)
                return null;
            return fList;
        }

        public static List<UPDirectory> DirectoryInfoToUP(DirectoryInfo[] dInfos)
        {
            List<UPDirectory> dList = new List<UPDirectory>();
            foreach (DirectoryInfo dInfo in dInfos)
                dList.Add(DirectoryInfoToUP(dInfo));
            if (dList.Count == 0)
                return null;
            return dList;
        }

        public static void Serialize(string filename, UPDirectory upDir)
        {
            XmlSerializer writer = new XmlSerializer(typeof(UPDirectory));

            var xns = new XmlSerializerNamespaces();
            xns.Add(string.Empty, string.Empty);

            StreamWriter file = new StreamWriter(filename);
            writer.Serialize(file, upDir, xns);
            file.Close();
        }

        public static UPDirectory DeSerialize(string filepath)
        {
            UPDirectory upDir = new UPDirectory();
            if (File.Exists(filepath))
            {
                StreamReader file = null;
                try
                {
                    XmlSerializer reader = new XmlSerializer(typeof(UPDirectory));
                    file = new StreamReader(filepath);
                    upDir = (UPDirectory)reader.Deserialize(file);
                    file.Close();
                }
                catch
                {
                    file.Close();
                    return null;
                }
                finally
                {
                    if (file != null)
                        file.Close();
                }
            }
            return upDir;
        }

        public static bool GenerateFiles(UPDirectory upDir, string root)
        {
            try
            {
                string cDir = Path.Combine(root, upDir.Name);
                DirectoryInfo di = Directory.CreateDirectory(Path.Combine(root, upDir.Name));
                di.Attributes = upDir.Attributes;
                di.CreationTime = upDir.CreationTime;
                di.LastAccessTime = upDir.LastAccessTime;
                di.LastWriteTime = upDir.LastWriteTime;
                foreach (UPFile file in upDir.Files)
                {
                    FileInfo fi = new FileInfo(Path.Combine(cDir, file.Name));
                    FileStream fs = fi.Create();
                    fs.SetLength(file.Length);
                    fs.Close();
                    fi.LastAccessTime = file.LastAccessTime;
                    fi.LastWriteTime = file.LastWriteTime;
                    fi.CreationTime = file.CreationTime;
                }
                foreach (UPDirectory upDir2 in upDir.Directories)
                {
                    if (!GenerateFiles(upDir2, cDir))
                        return false;
                }
            }
            catch { return false; }
            return true;
        }
    }
}
