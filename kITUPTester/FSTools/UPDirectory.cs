﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using System.IO;

namespace kITUPTester
{
    [XmlType(TypeName = "Directory")]
    public class UPDirectory
    {
        [Flags]
        public enum FSCondition
        {
            None = 0,
            FileAttributes = 1,
            FileCreationTime = 2,
            FileLastAccessTime = 4,
            FileLastWriteTime = 8,
            DirAttributes = 16,
            DirCreationTime = 32,
            DirLastAccessTime = 64,
            DirLastWriteTime = 128
        }

        [XmlAttribute("Name")]
        public string Name;

        [XmlAttribute("Attributes")]
        public FileAttributes Attributes;

        [XmlAttribute("CreationTime")]
        public DateTime CreationTime;

        [XmlAttribute("LastAccessTime")]
        public DateTime LastAccessTime;

        [XmlAttribute("LastWriteTime")]
        public DateTime LastWriteTime;

        [XmlElement("Directory")]
        public List<UPDirectory> Directories;

        [XmlElement("File")]
        public List<UPFile> Files;

        static bool cmpres = true;

        private bool CompareRecursive(string path, ref List<Note> Notes, UPDirectory cmDir, FSCondition cond)
        {
            path = Path.Combine(path, cmDir.Name);

            foreach (UPFile f in this.Files)
            {
                UPFile cmFile = cmDir.Files.Find(i => i.Name == f.Name);
                if (cmFile == null)
                {
                    cmpres = false;
                    List<string> Data = new List<string>();
                    Data.Add(Path.Combine(path, f.Name));
                    Notes.Add(new Note() { Kind = NoteKind.fs_f404, Data = Data, Type = NoteType.Error });
                }
                else
                {
                    if (f.Length != cmFile.Length)
                    {
                        cmpres = false;
                        List<string> Data = new List<string>();
                        Data.Add(Path.Combine(path, f.Name));
                        Notes.Add(new Note() { Kind = NoteKind.fs_wrFile, Data = Data, Type = NoteType.Error });
                    }
                    if (cond.HasFlag(FSCondition.FileAttributes))
                    {
                        if (f.Attributes != cmFile.Attributes)
                        {
                            cmpres = false;
                            List<string> Data = new List<string>();
                            Data.Add(Path.Combine(path, f.Name));
                            Data.Add(f.Attributes.ToString());
                            Data.Add(cmFile.Attributes.ToString());
                            Notes.Add(new Note() { Kind = NoteKind.fs_atFile, Data = Data, Type = NoteType.Error });
                        }
                    }
                    if (cond.HasFlag(FSCondition.FileCreationTime))
                    {
                        if (f.CreationTime != cmFile.CreationTime)
                        {
                            cmpres = false;
                            List<string> Data = new List<string>();
                            Data.Add(Path.Combine(path, f.Name));
                            Data.Add(f.CreationTime.ToString());
                            Data.Add(cmFile.CreationTime.ToString());
                            Notes.Add(new Note() { Kind = NoteKind.fs_ctFile, Data = Data, Type = NoteType.Error });
                        }
                    }
                    if (cond.HasFlag(FSCondition.FileLastAccessTime))
                    {
                        if (f.LastAccessTime != cmFile.LastAccessTime)
                        {
                            cmpres = false;
                            List<string> Data = new List<string>();
                            Data.Add(Path.Combine(path, f.Name));
                            Data.Add(f.LastAccessTime.ToString());
                            Data.Add(cmFile.LastAccessTime.ToString());
                            Notes.Add(new Note() { Kind = NoteKind.fs_latFile, Data = Data, Type = NoteType.Error });
                        }
                    }
                    if (cond.HasFlag(FSCondition.FileLastWriteTime))
                    {
                        if (f.LastWriteTime != cmFile.LastWriteTime)
                        {
                            cmpres = false;
                            List<string> Data = new List<string>();
                            Data.Add(Path.Combine(path, f.Name));
                            Data.Add(f.LastWriteTime.ToString());
                            Data.Add(cmFile.LastWriteTime.ToString());
                            Notes.Add(new Note() { Kind = NoteKind.fs_lwtFile, Data = Data, Type = NoteType.Error });
                        }
                    }
                }
            }

            foreach (UPDirectory d in this.Directories)
            {
                UPDirectory cmSubDir = cmDir.Directories.Find(i => i.Name == d.Name);
                if (cmSubDir == null)
                {
                    cmpres = false;
                    List<string> Data = new List<string>();
                    Data.Add(Path.Combine(path, d.Name) + "\\");
                    Notes.Add(new Note() { Kind = NoteKind.fs_d404, Data = Data, Type = NoteType.Error });
                }
                else
                {
                    if (cond.HasFlag(FSCondition.DirAttributes))
                    {
                        if (d.Attributes != cmSubDir.Attributes)
                        {
                            cmpres = false;
                            List<string> Data = new List<string>();
                            Data.Add(Path.Combine(path, d.Name) + "\\");
                            Data.Add(d.Attributes.ToString());
                            Data.Add(cmSubDir.Attributes.ToString());
                            Notes.Add(new Note() { Kind = NoteKind.fs_atDir, Data = Data, Type = NoteType.Error });
                        }
                    }
                    if (cond.HasFlag(FSCondition.DirCreationTime))
                    {
                        if (d.CreationTime != cmSubDir.CreationTime)
                        {
                            cmpres = false;
                            List<string> Data = new List<string>();
                            Data.Add(Path.Combine(path, d.Name) + "\\");
                            Data.Add(d.CreationTime.ToString());
                            Data.Add(cmSubDir.CreationTime.ToString());
                            Notes.Add(new Note() { Kind = NoteKind.fs_ctDir, Data = Data, Type = NoteType.Error });
                        }
                    }
                    if (cond.HasFlag(FSCondition.DirLastAccessTime))
                    {
                        if (d.LastAccessTime != cmSubDir.LastAccessTime)
                        {
                            cmpres = false;
                            List<string> Data = new List<string>();
                            Data.Add(Path.Combine(path, d.Name) + "\\");
                            Data.Add(d.LastAccessTime.ToString());
                            Data.Add(cmSubDir.LastAccessTime.ToString());
                            Notes.Add(new Note() { Kind = NoteKind.fs_latDir, Data = Data, Type = NoteType.Error });
                        }
                    }
                    if (cond.HasFlag(FSCondition.DirLastWriteTime))
                    {
                        if (d.LastWriteTime != cmSubDir.LastWriteTime)
                        {
                            cmpres = false;
                            List<string> Data = new List<string>();
                            Data.Add(Path.Combine(path, d.Name) + "\\");
                            Data.Add(d.LastWriteTime.ToString());
                            Data.Add(cmSubDir.LastWriteTime.ToString());
                            Notes.Add(new Note() { Kind = NoteKind.fs_lwtDir, Data = Data, Type = NoteType.Error });
                        }
                    }
                    d.CompareRecursive(path, ref Notes, cmSubDir, cond);
                }
            }

            return false;
        }
        public bool Compare(ref List<Note> Notes, UPDirectory upDir, FSCondition cond)
        {
            cmpres = true;
            CompareRecursive("\\", ref Notes, upDir, cond);
            return cmpres;
        }
    }
}
