﻿using System;
using System.Xml.Serialization;
using System.IO;

namespace kITUPTester
{
    [XmlType(TypeName = "File")]
    public class UPFile
    {
        [XmlAttribute("Name")]
        public string Name;

        [XmlAttribute("Length")]
        public long Length;

        [XmlAttribute("Attributes")]
        public FileAttributes Attributes;

        [XmlAttribute("CreationTime")]
        public DateTime CreationTime;

        [XmlAttribute("LastAccessTime")]
        public DateTime LastAccessTime;

        [XmlAttribute("LastWriteTime")]
        public DateTime LastWriteTime;
    }
}
